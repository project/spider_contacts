<?php
/**
 * @file
 * Spider Contacts module global options.
 */

/**
 * Menu loader callback. Load global options.
 */
function spider_contacts_global_options() {
  if (db_query("SELECT * FROM {spider_contacts_params}")->fetchObject()) {
    $row = db_query("SELECT * FROM {spider_contacts_params}")->fetchObject();
    $choose_category = $row->choose_category;
    $name_search = $row->name_search;
    $enable_message = $row->enable_message;
    $show_email = $row->show_email;
    $show_name = $row->show_name;
    $show_phone = $row->show_phone;
    $show_cont_pref = $row->show_cont_pref;
  }
  else {
    $choose_category = 1;
    $name_search = 1;
    $enable_message = 1;
    $show_email = 1;
    $show_name = 1;
    $show_phone = 1;
    $show_cont_pref = 1;
  }
  $form = array();
  $free_version = '
    <a href="http://web-dorado.com/drupal-contacts-guide-step-5/step-5-1.html" target="_blank" style="float:right;"><img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/user-manual.png" border="0" alt="' . t('User Manual') . '"></a><div style="clear:both;"></div>
    <div class="messages warning">Global Options are disabled in free version. If you need this functionality, you need to buy the commercial version.</div>
    <a href="http://web-dorado.com/products/drupal-contacts-module.html" target="_blank" style="color:red; text-decoration:none; float:right; width:100%">
                    <img style="float:right; clear:both;" src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/header.png" border="0" alt="www.web-dorado.com" width="215"><br />
                  <div style="float:right; clear:both;">' . t('Get the full version') . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
                  </a>';
  $form['fieldset_global_options'] = array(
    '#type' => 'fieldset',
    '#prefix' => $free_version,
    '#title' => t('Global Options'),
  );
  $form['fieldset_global_options']['choose_category'] = array(
    '#type' => 'radios',
    '#title' => t('Search Contact on frontend by category'),
    '#default_value' => $choose_category,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['fieldset_global_options']['name_search'] = array(
    '#type' => 'radios',
    '#title' => t('Search Contact on frontend by name'),
    '#default_value' => $name_search,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['fieldset_message_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Message Options'),
  );
  $form['fieldset_message_options']['enable_message'] = array(
    '#type' => 'radios',
    '#title' => t('Opportunity to Send a Message'),
    '#default_value' => $enable_message,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['fieldset_message_options']['show_email'] = array(
    '#type' => 'radios',
    '#title' => t('"Email" Field'),
    '#default_value' => $show_email,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['fieldset_message_options']['show_name'] = array(
    '#type' => 'radios',
    '#title' => t('"Name" Field'),
    '#default_value' => $show_name,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['fieldset_message_options']['show_phone'] = array(
    '#type' => 'radios',
    '#title' => t('"Phone" Field'),
    '#default_value' => $show_phone,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['fieldset_message_options']['show_cont_pref'] = array(
    '#type' => 'radios',
    '#title' => t('"Contact Preferences" Field'),
    '#default_value' => $show_cont_pref,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['global_options_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('onclick' => 'alert(Drupal.t("You cant save the changes in free version."));return false;'),
  );
  return $form;
}
