<?php
/**
 * @file
 * This file contains all functions which need for contact view in cells.
 */
/**
 * View contacts in cells.
 */
function spider_contacts_contact_cells($nodeid, $theme_id, $category_id) {
  // Parameters for SpiderBox.
  drupal_add_js(array(
    'spider_contacts' => array(
      'delay' => 3000,
      'allImagesQ' => 0,
      'slideShowQ' => 0,
      'darkBG' => 1,
      'juriroot' => base_path() . drupal_get_path('module', 'spider_contacts'),
      'spiderShop' => 1,
    ),
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/spiderBox/spiderBox.js');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_common.js');
  drupal_add_css(drupal_get_path('module', 'spider_contacts') . '/css/spider_contacts_main.css');
  if (isset($_GET['page_num'])) {
    $page_num = $_GET['page_num'];
  }
  else {
    $page_num = 1;
  }
  if (isset($_POST['name_search'])) {
    $name_search = check_plain($_POST['name_search']);
  }
  else {
    $name_search = '';
  }
  
  $cat_id = 0;
  if (isset($_POST['cat_id'])) {
    $cat_id = check_plain($_POST['cat_id']);
  }
  else {
    $cat_id = $category_id;
  }
  // If category has deleted, show all contacts.
  if (!db_query("SELECT id FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $cat_id))->fetchField()) {
    $cat_id = 0;
  }
  if ($cat_id == 0) {
    $compare = '>=';
  }
  else {
    $compare = '=';
  }
  $page_link = url('node/' . $nodeid, array('query' => array('example' => 1), 'absolute' => TRUE));
  $content = '';
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  if (!$theme) {
    $theme_id = 1;
    db_query("UPDATE {spider_contacts_form_table} SET theme=:theme WHERE vid=:vid", array(':theme' => 1, ':vid' => $nodeid));
  }
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  $params = db_query("SELECT * FROM {spider_contacts_params} WHERE id=:id", array(':id' => 1))->fetchObject();
  $category_list = db_query("SELECT id,name FROM {spider_contacts_contacts_categories} WHERE published=:published ORDER BY ordering", array(':published' => 1))->fetchAllKeyed();
  $prod_in_page = $theme->count_of_contact_in_the_row * $theme->count_of_rows_in_the_page;
  $rows = db_query("SELECT id FROM {spider_contacts_contacts} WHERE published=:published AND category_id" . $compare . ":category_id AND (first_name LIKE :first_name OR last_name LIKE :last_name) ORDER BY ordering LIMIT " . (($page_num - 1) * $prod_in_page) . ", " . $prod_in_page, array(':published' => 1, ':category_id' => $cat_id, ':first_name' => '%' . db_like($name_search) . '%', ':last_name' => '%' . db_like($name_search) . '%'))->fetchCol();
  $prod_count = db_query("SELECT COUNT(id) FROM {spider_contacts_contacts} WHERE published=1 AND category_id" . $compare . ":category_id AND (first_name LIKE :first_name OR last_name LIKE :last_name)", array(':category_id' => $cat_id, ':first_name' => '%' . db_like($name_search) . '%', ':last_name' => '%' . db_like($name_search) . '%'))->fetchField();
  $permalink_for_sp_cat = url('spider_contacts/contact_view', array(
    'query' => array('theme_id' => $theme_id),
    'absolute' => TRUE
  ));
  $prod_iterator = 0;
  drupal_add_js('
    function spider_contacts_submit_catal(page_link) {
      if (document.getElementById("cat_form")) {
          document.getElementById("cat_form").setAttribute("action", page_link);
          document.getElementById("cat_form").submit();
        }
        else {
          window.location.href = page_link;
        }
    }', array('type' => 'inline'));
  if ($theme->radius) {
    $border_radius = '8px';
  }
  else {
    $border_radius = '0';
  }
  $content .= '
    <style type="text/css">
      .spidercontactparamslist {
        margin: 0 !important;
        padding: 0 !important;
      }
      #tdviewportheight {
        border: none;
      }
      #paramstable td span,
      #paramstable td ul,
      #paramstable td li {
        list-style: none;
        padding: 3px 0 3px 10px;
      }
      #contactMainDiv th {
        text-transform: none !important;
        text-align: center !important;
      }
      #contactMainDiv,
      .spidercontactbutton,
      .spidercontactinput {
        -webkit-border-radius: ' . $border_radius . ';
        -moz-border-radius: ' . $border_radius . ';
        border-radius: ' . $border_radius . ';
      }
      #contactMainDiv #contTitle {
        -webkit-border-top-right-radius: ' . $border_radius . ';
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        -moz-border-radius-topleft: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
        padding: 5px;
      }
      #contactMainDiv th {
        text-transform: none !important;
        text-align: center !important;
      }
      #contactMainDiv {
        margin: 15px;
        padding: 5px;
        text-align: left;
      }
      #contactssMainDivs, .spidercontactbutton, .spidercontactinput {
        font-size: 12px !important;
        -webkit-border-radius: ' . $border_radius . ' !important;
        -moz-border-radius: ' . $border_radius . ' !important;
        border-radius: ' . $border_radius . ' ! imp .0 ortant;

      }
      .spidercontactinput {
        cursor: text !important;
        font-size: 12px !important;
        -webkit-border-radius: ' . $border_radius . ';
        -moz-border-radius: ' . $border_radius . ';
      }
      Select.spidercontactinput {
        padding-top: 3px !important;
        padding-bottom: 3px !important;
      }
      .ContactSearchBox,
      spidercontactbutton,
      .input {
        -webkit-border-radius: ' . $border_radius . ';
        -moz-border-radius: ' . $border_radius . ';
        border-radius: ' . $border_radius . ';
        padding-bottom: 5px !important;
        padding-left: 5px !important;
        padding-top: 5px !important;
        padding-right: 5px !important;
        margin-left: 5px !important;
        line-height: 1 !important;
        text-align: right !important;
      }
      .ContactSearchBox .input {
        padding-bottom: 0px !important;
        padding-top: 0px !important;
        padding-left: 3px !important;
        padding-right: 3px !important;
        margin-left: 3px !important;
      }
      #contactssMainDivs #contTitle {
        -webkit-border-top-right-radius: ' . $border_radius . ';
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        -moz-border-radius-topleft: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
      }
      #contactssMainDivs,
      .spidercontactbutton {
        font-size: 10px !important;
        -webkit-border-radius: ' . $border_radius . ' !important;
        -moz-border-radius: ' . $border_radius . ' !important;
        border-radius: ' . $border_radius . ' !important;
      }
      #contactssMainDivs input,
      #contactssMainDivs textarea {
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -webkit-transition: all 0.2s linear;
        -webkit-transition-delay: 0s;
        -moz-transition: all 0.2s linear 0s;
        -o-transition: all 0.2s linear 0s;
        transition: all 0.2s linear 0s;
      }
      #contactssMainDivs textarea,
      #contactssMainDivs input[type="text"],
      #contactssMainDivs select {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-background-clip: padding;
        -moz-background-clip: padding;
        background-clip: padding-box;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-appearance: none;
        background-color: white;
        border: 1px solid #cccccc;
        color: black;
        outline: 0;
        margin: 0;
        padding: 3px 4px;
        text-align: left;
        font-size: 13px;
        font-family: Arial, "Liberation Sans", FreeSans, sans-serif;
        height: 2.2em;
        vertical-align: top;
        *padding-top: 2px;
        *padding-bottom: 1px;
        *height: auto;
      }
      #contactssMainDivs textarea[disabled],
      #contactssMainDivs input[type="text"][disabled] {
        background-color: #eeeeee;
      }
      #contactssMainDivs textarea:focus,
      #contactssMainDivs input[type="text"]:focus {
        border-color: rgba(82, 168, 236, 0.8);
        outline: 0;
        outline: thin dotted \9;
        /* IE6-9 */
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
      }
      #contactssMainDivs input[disabled],
      #contactssMainDivs textarea[disabled] {
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        -moz-user-select: -moz-none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        user-select: none;
        color: #888888;
        cursor: default;
      }
      #contactssMainDivs input::-webkit-input-placeholder,
      #contactssMainDivs textarea::-webkit-input-placeholder {
        color: #888888;
      }
      #contactssMainDivs input:-moz-placeholder,
      #contactssMainDivs textarea:-moz-placeholder {
        color: #888888;
      }
      #contactssMainDivs input.placeholder_text,
      #contactssMainDivs textarea.placeholder_text {
        color: #888888;
      }
      #contactssMainDivs textarea {
        min-height: 80px;
        overflow: auto;
        padding: 5px;
        resize: vertical;
        width: 100%;
      }
      #contactssMainDivs optgroup {
        color: black;
        font-style: normal;
        font-weight: normal;
        font-family: Arial, "Liberation Sans", FreeSans, sans-serif;
      }
      #contactssMainDivs optgroup::-moz-focus-inner {
        border: 0;
        padding: 0;
      }
      #contactMainTable {
        padding-top: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        padding-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
        margin-right: 0px;
        margin-top: 0px;
        border: none !important;
        border-collapse: inherit;
      }
      #contactMainTable table,
      #contactMainTable tbody,
      #contactMainTable tr,
      #contactMainTable td {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
        padding-left: 0px !important;
        padding-right: 0px !important;
        margin-bottom: 0px !important;
        margin-left: 0px !important;
        margin-right: 0px !important;
        margin-top: 0px !important;
        padding: inherit;
        border: none !important;
        border-collapse: inherit;
        opacity: 1 !important;
        text-align: left;
        overflow: hidden !important;
      }
      #contactMainTable div {
        overflow: hidden !important;
      }
      #contactMainTable span {
        overflow: hidden !important;
      }
      .ContactSearchBox input {
        margin-top: -3px !important;
        padding: 4px;
      }
      .S_P_contactssMainDivs table,
      .S_P_contactssMainDivs tr,
      .S_P_contactssMainDivs td,
      .S_P_contactssMainDivs tbody,
      .S_P_contactssMainDivs table tr td {
        border: none !important;
      }
      #contactssMainDivs ul li,
      #contactssMainDivs ul,
      #contactssMainDivs li {
        list-style-type: none !important;
      }
      #contactssMainDivs td,
      #contactssMainDivs tr,
      #contactssMainDivs tbody,
      #contactssMainDivs div {
        line-height: inherit !important;
        background-color: inherit;
        color: inherit;
        opacity: inherit !important;
        max-width: inherit !important;
        max-height: inherit !important;
      }
      #contactssMainDivs,
      #contactssMainDivs div {
        max-width: 1000000px !important;
      }
      #contMiddle,
      #contMiddle a,
      #contMiddle li,
      #contMiddle ol {
        background-color: inherit !important;
      }
      #tdviewportheight div,
      #tdviewportheight td,
      #tdviewportheight {
        vertical-align: middle;
      }
    </style>';
  $a = 'form';
  if ($params->choose_category || $params->name_search) {
    $content .= '
    <div id="contactssMainDivs">
      <' . $a . ' action="' . $page_link . '" method="post" name="cat_form" id="cat_form">
        <input type="hidden" name="page_num" value="1">
        <div class="ContactSearchBox">';
    if ($params->choose_category && ($category_id == 0)) {
      $content .= '
          <span style="font-size:14px !important">' . t('Choose Category') . '</span>&nbsp;
          <select id="cat_id" name="cat_id" class="spidercontactinput" size="1" onChange="document.cat_form.submit();">
            <option value="0">' . t('All') . '</option> ';
      foreach ($category_list as $key => $category_name) {
        if ($key == $cat_id) {
          $content .= '
            <option value="' . $key . '"  selected="selected">' . $category_name . '</option>';
        }
        else {
          $content .= '
            <option value="' . $key . '" >' . $category_name . '</option>';
        }
      }
      $content .= '
          </select>';
    }
    if ($params->name_search) {
      if (isset($_POST['name_search'])) {
        $name_search = check_plain($_POST['name_search']);
      }
      else {
        $name_search = '';
      }
      $content .= '
          <br /><br />
          <span style="font-size:14px !important">' . t('Search by name') . '</span>&nbsp;
          <input type="text" id="name_search" name="name_search" class="spidercontactinput" value="' . $name_search . '">
          <input type="submit" value="' . t('Search') . '" class="spidercontactbutton" style="background-color:#' . $theme->button_background_color . '; color:#' . $theme->button_color . '; width:inherit;">
          <input type="button" value="' . t('Reset') . '" onClick="spider_contacts_cat_form_reset(this.form);" class="spidercontactbutton" style="background-color:#' . $theme->button_background_color . '; color:#' . $theme->button_color . '; width:inherit;">';
    }
    $content .= '
        </div>
      </form>';
  }
  $content .= '
      <table border="0" cellpadding="0" cellspacing="0" id="contactMainTable">
        <tr>';
  foreach ($rows as $row) {
    $row_param = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $row))->fetchObject();
    if ((($prod_iterator % $theme->count_of_contact_in_the_row) === 0) && ($prod_iterator > 0)) {
      $content .= '
        </tr>
        <tr>';
    }
    $prod_iterator++;
    $link = $permalink_for_sp_cat . '&contact_id=' . $row_param->id . '&page_num=' . $page_num . '&back=' . $nodeid;
    $imgurl = explode('#***#', $row_param->image_url);
    $content .= '
          <td>
            <div id="contactMainDiv" style="border-width:' . $theme->border_width . 'px; border-color:#' . $theme->border_color . ';border-style:' . $theme->border_style . ';' . (($theme->text_size_small != '') ? ('font-size:' . $theme->text_size_small . 'px;') : '') . (($theme->text_color != '') ? ('color:#' . $theme->text_color . ';') : '') . (($theme->module_background_color != '') ? ('background-color:#' . $theme->module_background_color . ';') : '') . ' width:' . $theme->contact_cell_width . 'px; height:' . $theme->contact_cell_height . 'px; ">
              <div style="height:' . ($theme->contact_cell_height - 20) . 'px;">
                <div id="contTitle" style="font-size:' . $theme->title_size_small . 'px;' . (($theme->title_color != '') ? ('color:#' . $theme->title_color . ';') : '') . (($theme->title_background_color != '') ? ('background-color:#' . $theme->title_background_color . ';') : '') . '">
                  <a style="' . (($theme->title_color != '') ? ('color:#' . $theme->title_color . ';') : '') . '" href="' . $link . '">' . $row_param->first_name . ' ' . $row_param->last_name . '</a>
                </div>
                <table id="contMiddle" border="0" cellspacing="0" cellpadding="0">
                  <tr>';
      if (!($row_param->image_url != '')) {
        $imgurl[0] = base_path() . drupal_get_path('module', 'spider_contacts') . '/images/no_image.jpg';
        $content .= '
                    <td style="padding:10px;">
                      <img style="margin:10px; max-width:' . $theme->small_picture_width . 'px;max-height:' . $theme->small_picture_height . 'px" src="' . $imgurl[0] . '" />
                    </td>';
      }
      else {
        $content .= '
                    <td style="padding:10px;">
                      <a href="' . $imgurl[0] . '" target="_blank">
                        <img style="margin:10px; max-width:' . $theme->small_picture_width . 'px; max-height:' . $theme->small_picture_height . 'px" src="' . $imgurl[0] . '" />
                      </a>
                    </td>';
        
      }
      
      if ($row_param->category_id == 0) {
        $category_title = t('Uncategorized');
      }
      else {
        $category_title = $category_list[$row_param->category_id];
      }
      $content .= '
                  </tr>
                  <tr>
                    <td>
                      <table cellspacing="0" cellpadding="5" ' . (($theme->module_background_color != '') ? ('bordercolor="#' . $theme->module_background_color . '"') : ' ') . ' id="paramstable" border="1" width="100%" style="border-style:solid; width:' . $theme->parameters_select_box_width . 'px; ">';
      if ($row_param->category_id > 0) {
        $content .= '   <tr style="' . (($theme->params_background_color2 != '') ? ('background-color:#' . $theme->params_background_color2 . ';') : '') . ' vertical-align:middle;">
                          <td><b>' . t('Category:') . '</b></td>
                          <td style="' . (($theme->params_color != '') ? ('color:#' . $theme->params_color . ';') : '') . '">
                            <span id="cat_' . $row_param->id . '">' . $category_title . '</span>
                          </td>
                        </tr>';
      }
      else {
        $content .= '   <span id="cat_' . $row_param->id . '"></span>';
      }
      if ($row_param->email != '') {
        $content .= '   <tr style="' . (($theme->params_background_color1 != '') ? ('background-color:#' . $theme->params_background_color1 . ';') : '') . ' vertical-align:middle;">
                          <td><b>' . t('Email') . '</b></td>
                          <td style="' . (($theme->params_color != '') ? ('color:#' . $theme->params_color . ';') : '') . '">
                            <span id="email_' . $row_param->id . '">
                              <a href="mailto:' . $row_param->email . '">' . $row_param->email . '</a>
                            </span>
                          </td>
                        </tr>';
      }
      else {
        $content .= '   <span id="email_' . $row_param->id . '"></span>';
      }
      $par_data = explode("#@@@#", $row_param->param);
      for ($j = 0; $j < count($par_data); $j++)
        if ($par_data[$j] != '') {
          $par1_data = explode("@@:@@", $par_data[$j]);
          $par_values = explode("#***#", $par1_data[1]);
          $countOfPar = 0;
          for ($k = 0; $k < count($par_values); $k++) {
            if ($par_values[$k] != "") {
              $countOfPar++;
            }
          }
          $bgcolor = (($j % 2) ? (($theme->params_background_color2 != '') ? ('background-color:#' . $theme->params_background_color2 . ';') : '') : (($theme->params_background_color1 != '') ? ('background-color:#' . $theme->params_background_color1 . ';') : ''));
          if ($countOfPar != 0) {
            $content .= '
                        <tr style="' . $bgcolor . 'text-align:left">
                          <td><b>' . $par1_data[0] . ':</b></td>
                          <td style="' . (($theme->text_size_small != '') ? ('font-size:' . $theme->text_size_small . 'px;') : '') . $bgcolor . (($theme->params_color != '') ? ('color:#' . $theme->params_color . ';') : '') . 'width:' . $theme->parameters_select_box_width . 'px;">
                            <ul class="spidercontactparamslist">';
            for ($k = 0; $k < count($par_values); $k++) {
              if ($par_values[$k] != "") {
                if (preg_match('/(http:\/\/[^\s]+)/', $par_values[$k], $text)) {
                  $hypertext = "<a target=\"_blank\" href=\"" . $text[0] . "\">" . $text[0] . "</a>";
                  $newString = preg_replace('/(http:\/\/[^\s]+)/', $hypertext, $par_values[$k]);
                  $content .= '
                              <li>' . $newString . '</li>';
                }
                elseif (preg_match('/(https:\/\/[^\s]+)/', $par_values[$k], $text)) {
                  $hypertext = "<a target=\"_blank\" href=\"" . $text[0] . "\">" . $text[0] . "</a>";
                  $newString = preg_replace('/(https:\/\/[^\s]+)/', $hypertext, $par_values[$k]);
                  $content .= '
                              <li>' . $newString . '</li>';
                }
                elseif ($par_values[$k]) {
                  $content .= '
                              <li>' . $par_values[$k] . '</li>';
                }
              }
            }
            $content .= '   </ul>
                          </td>
                        </tr>';
          }
        }
      $description = $row_param->short_description;
      $content .= '   </table>
                      <div id="contDescription">' . $description . '</div>
                    </td>
                  </tr>
                </table>
              </div>
              <div id="contMore">
                <a href="' . $link . '" style="' . (($theme->hyperlink_color != '') ? ('color:#' . $theme->hyperlink_color . ';') : '') . '">' . t('More') . '</a>
              </div>
            </div>
          </td>';
    }
    $content .= '
        </tr>
      </table>
      <div id="spidercontactnavigation" style="text-align:center;">';
    if ($cat_id != 0) {
      $page_link .= "&cat_id=" . $cat_id;
    }
    if ($name_search != "") {
      $page_link .= "&name_search=" . $name_search;
    }
    if (($prod_count > $prod_in_page) && ($prod_in_page > 0)) {
      $r = ceil($prod_count / $prod_in_page);
      $navstyle = (($theme->text_size_small != '') ? ('font-size:' . $theme->text_size_small . 'px !important;') : 'font-size:12px !important;') . (($theme->text_color != '') ? ('color:#' . $theme->text_color . ' !important;') : '');
      $link = $page_link . '&page_num= ';
      if ($page_num > 5) {
        $link = $page_link . '&page_num=1';
        $content .= '
          &nbsp;&nbsp;
          <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('First') . '</a>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;...&nbsp;';
      }
      if ($page_num > 1) {
        $link = $page_link . '&page_num=' . ($page_num - 1);
        $content .= '
          &nbsp;&nbsp;
          <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Prev') . '</a>&nbsp;&nbsp;';
      }
      for ($i = $page_num - 4; $i < ($page_num + 5); $i++) {
        if ($i <= $r and $i >= 1) {
          $link = $page_link . '&page_num=' . $i;
          if ($i == $page_num) {
            $content .= '
          <span style="font-weight:bold;color:#000000">&nbsp;' . $i . '&nbsp;</span>';
          }
          else {
            $content .= '
          <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">&nbsp;' . $i . '&nbsp;</a>';
          }
        }
      }
      if ($page_num < $r) {
        $link = $page_link . '&page_num=' . ($page_num + 1);
        $content .= '
          &nbsp;&nbsp;
          <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Next') . '</a>&nbsp;&nbsp;';
      }
      if (($r - $page_num) > 4) {
        $link = $page_link . '&page_num=' . $r;
        $content .= '
          &nbsp;...&nbsp;&nbsp;&nbsp;
          <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Last') . '</a>';
      }
    }
    /*$pos = strpos($_SERVER['QUERY_STRING'], "page_num") - 1;
    if ($pos > 0) {
      $url = substr($_SERVER['QUERY_STRING'], 0, $pos);
    }
    else {
      $url = $_SERVER['QUERY_STRING'];
    }
    $pos = strpos($_SERVER['QUERY_STRING'], "cat_id") - 1;
    if ($pos > 0) {
      $url = substr($url, 0, $pos);
    }
    if ($cat_id != 0) {
      $url .= "&cat_id=" . $cat_id;
    }


  if ($cont_count > $cont_in_page and $cont_in_page > 0) {
    $r = ceil($cont_count / $cont_in_page);
    $navstyle = 'cursor: pointer;' . (($params->get('text_size_small') != '') ? ('font-size:' . $params->get('text_size_small') . 'px;') : '') . (($params->get('text_color') != '') ? ('color:' . $params->get('text_color') . ';') : '');
    if (strpos(get_permalink(), '?'))
      $link = get_permalink() . '&page_num= ';
    else
      $link = get_permalink() . '?page_num= ';
    if ($page_num > 5) {
      if (strpos(get_permalink(), '?'))
        $link = get_permalink() . '&page_num=1';
      else
        $link = get_permalink() . '?page_num=1';
      echo "

&nbsp;&nbsp;<a onclick=\"submit_cotctt('{$link}')\" style=\"$navstyle\">first</a>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;...&nbsp";
    }
    if ($page_num > 1) {
      if (strpos(get_permalink(), '?'))
        $link = get_permalink() . '&page_num=' . ($page_num - 1);
      else
        $link = get_permalink() . '?page_num=' . ($page_num - 1);
      echo "&nbsp;&nbsp;<a onclick=\"submit_cotctt('{$link}')\" style=\"$navstyle\">prev</a>&nbsp;&nbsp;";
    }
    for ($i = $page_num - 4; $i < ($page_num + 5); $i++) {
      if ($i <= $r and $i >= 1) {
        if (strpos(get_permalink(), '?'))
          $link = get_permalink() . '&page_num=' . $i;
        else
          $link = get_permalink() . '?page_num=' . $i;
        if ($i == $page_num)
          echo "<span style='font-size:" . (((int)$params->get("cube_text_size_small") + 2) . "px") . "; font-weight:bold;color:#000000'>&nbsp;$i&nbsp;</span>";
        else
          echo "<a onclick=\"submit_cotctt('{$link}')\" style=\"$navstyle\">&nbsp;$i&nbsp;</a>";
      }
    }
    if ($page_num < $r) {
      if (strpos(get_permalink(), '?'))
        $link = get_permalink() . '&page_num=' . ($page_num + 1);
      else
        $link = get_permalink() . '?page_num=' . ($page_num + 1);
      echo "&nbsp;&nbsp;<a onclick=\"submit_cotctt('{$link}')\" style=\"$navstyle\">next</a>&nbsp;&nbsp;";
    }
    if (($r - $page_num) > 4) {
      if (strpos(get_permalink(), '?'))
        $link = get_permalink() . '&page_num=' . $r;
      else
        $link = get_permalink() . '?page_num=' . $r;
      echo "&nbsp;...&nbsp;&nbsp;&nbsp;<a href= onclick=\"submit_cotctt('{$link}')\" style=\"$navstyle\">last</a>";
    }
  }*/

  $content .= '
      </div>
    </div>';
  drupal_add_js('var SpiderCatOFOnLoad = window.onload; window.onload = SpiderCatAddToOnload;', array('type' => 'inline', 'scope' => 'footer'));
  return $content;
}
