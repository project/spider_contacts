<?php
/**
 * @file
 * This file contains all functions which need for calendar view.
 */
/**
 * View products in cells.
 */
function spider_contacts_contact_short($nodeid, $theme_id, $category_id) {
   // Parameters for SpiderBox.
  drupal_add_js(array(
    'spider_contacts' => array(
      'delay' => 3000,
      'allImagesQ' => 0,
      'slideShowQ' => 0,
      'darkBG' => 1,
      'juriroot' => base_path() . drupal_get_path('module', 'spider_contacts'),
      'spiderShop' => 1,
    ),
    ),
    'setting');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/spiderBox/spiderBox.js');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_common.js');
  drupal_add_css(drupal_get_path('module', 'spider_contacts') . '/css/spider_contacts_main.css');
  if (isset($_GET['page_num'])) {
    $page_num = $_GET['page_num'];
  }
  else {
    $page_num = 1;
  }
  if (isset($_POST['name_search'])) {
    $name_search = check_plain($_POST['name_search']);
  }
  else {
    $name_search = '';
  }
  $cat_id = 0;
  if (isset($_POST['cat_id'])) {
    $cat_id = check_plain($_POST['cat_id']);
  }
  else {
    $cat_id = $category_id;
  }
  // If category has deleted, show all contacts.
  if (!db_query("SELECT id FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $cat_id))->fetchField()) {
    $cat_id = 0;
  }
  if ($cat_id == 0) {
    $compare = '>=';
  }
  else {
    $compare = '=';
  }
  $page_link = url('node/' . $nodeid, array(
    'query' => array('example' => 1),
    'absolute' => TRUE
  ));
  $content = '';
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  if (!$theme) {
    $theme_id = 1;
    db_query("UPDATE {spider_contacts_form_table} SET theme=:theme WHERE vid=:vid", array(
      ':theme' => 1,
      ':vid' => $nodeid
    ));
  }
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  $params = db_query("SELECT * FROM {spider_contacts_params} WHERE id=:id", array(':id' => 1))->fetchObject();
  $category_list = db_query("SELECT id,name FROM {spider_contacts_contacts_categories} WHERE published=:published ORDER BY ordering", array(':published' => 1))->fetchAllKeyed();
  $prod_in_page = $theme->cube_count_of_contact_in_the_row * $theme->cube_count_of_rows_in_the_page;
  $rows = db_query("SELECT id FROM {spider_contacts_contacts} WHERE published=:published AND category_id" . $compare . ":category_id AND (first_name LIKE :first_name OR last_name LIKE :last_name) ORDER BY ordering LIMIT " . (($page_num - 1) * $prod_in_page) . ", " . $prod_in_page, array(
    ':published' => 1,
    ':category_id' => $cat_id,
    ':first_name' => '%' . db_like($name_search) . '%',
    ':last_name' => '%' . db_like($name_search) . '%',
  ))->fetchCol();
  $prod_count = db_query("SELECT COUNT(id) FROM {spider_contacts_contacts} WHERE published=1 AND category_id" . $compare . ":category_id AND (first_name LIKE :first_name OR last_name LIKE :last_name)", array(
    ':category_id' => $cat_id,
    ':first_name' => '%' . db_like($name_search) . '%',
    ':last_name' => '%' . db_like($name_search) . '%',
  ))->fetchField();
  $permalink_for_sp_cat = url('spider_contacts/contact_view', array(
    'query' => array('theme_id' => $theme_id),
    'absolute' => TRUE
  ));
  $prod_iterator = 0;
  drupal_add_js('
    function spider_contacts_submit_catal(page_link) {
      if (document.getElementById("cat_form")) {
          document.getElementById("cat_form").setAttribute("action", page_link);
          document.getElementById("cat_form").submit();
        }
        else {
          window.location.href = page_link;
        }
    }', array('type' => 'inline'));
  if ($theme->cube_radius) {
    $border_radius = '8px';
  }
  else {
    $border_radius = '0';
  }
  $content .= '
  <style type="text/css">
    .ContactSearchBox select,
    .ContactSearchBox input[type="text"] {
      -moz-box-sizing: border-box;
      background-clip: padding-box;
      background-color: white;
      border: 1px solid #CCCCCC;
      border-radius: 3px 3px 3px 3px;
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
      color: black;
      font-family: Arial,"Liberation Sans",FreeSans,sans-serif;
      font-size: 13px;
      margin: 0;
      outline: 0 none;
      padding: 3px 4px;
      text-align: left;
    }
    .ContactSearchBox select:focus,
    .ContactSearchBox input[type="text"]:focus {
      border-color: rgba(82, 168, 236, 0.8);
      outline: 0;
      outline: thin dotted \9;
      /* IE6-9 */
      -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
      -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
      box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
    }
    #contMiddle td {
      border:none;
    }
    .spidercontactparamslist {
      margin: 0 !important;
      padding: 0 !important;
    }
    #tdviewportheight {
      border: none;
    }
    #contactMainTable tr,
    #contactMainTable tr {
      background-color: rgba(0,0,0,0);
    }
    #contactMainTable tbody {
      border: none;
    }
    #contactMainTable td {
      padding: 5px;
    }
    #tdviewportheight div,
    #tdviewportheight td,
    #tdviewportheight {
      vertical-align: middle;
    }
    #contactssMainDivs {
      vertical-align: middle !important;
    }
    #contMiddle td,
    contMiddle tr {
      border-top: none !important;
    }
    #contactMainDivCube,
    .spidercontactbutton {
      -webkit-border-radius: ' . $border_radius . ';
      -moz-border-radius: ' . $border_radius . ';
      border-radius: ' . $border_radius . ';
      padding: 5px;
    }
    #contMiddle {
      border: none !important;
    }
    #contactMainDiv th {
      text-transform: none !important;
      text-align: center !important;
    }
    #contactMainDivCube #contTitle {
      -webkit-border-top-right-radius: ' . $border_radius . ';
      -webkit-border-top-left-radius: ' . $border_radius . ';
      -moz-border-radius-topright: ' . $border_radius . ';
      -moz-border-radius-topleft: ' . $border_radius . ';
      border-top-right-radius: ' . $border_radius . ';
      border-top-left-radius: ' . $border_radius . ';
    }
    #ContactSearchBox,
    .spidercontactbutton {
      font-size: 10px !important;
      -webkit-border-radius: ' . $border_radius . ' !important;
      -moz-border-radius: ' . $border_radius . ' !important;
      border-radius: ' . $border_radius . ' !important;
      padding: 4px;
    }
    .input {
      text-align: left !important;
      font-size: 12px !important;
      -webkit-border-radius: ' . $border_radius . ' !important;
      -moz-border-radius: ' . $border_radius . ' !important;
      border-radius: ' . $border_radius . ' !important;
    }
    #ContactSearchBox {
      margin-bottom: 10px !important;
      text-align: right !important;
    }
  </style>';
  $a = 'form';
  if ($params->choose_category || $params->name_search) {
    $content .= '
  <' . $a . ' action="' . $page_link . '" method="post" name="cat_form" id="cat_form" style="text-align:right;">
    <input type="hidden" name="page_num" value="1">
    <div class="ContactSearchBox">';
    if ($params->choose_category && ($category_id == 0)) {
      $content .= '
      <span style="font-size:14px !important">' . t('Choose Category') . '</span>&nbsp;
      <select id="cat_id" name="cat_id" class="spidercontactinput" size="1" onChange="document.cat_form.submit();">
        <option value="0">' . t('All') . '</option> ';
      foreach ($category_list as $key => $category_name) {
        if ($key == $cat_id) {
          $content .= '
        <option value="' . $key . '"  selected="selected">' . $category_name . '</option>';
        }
        else {
          $content .= '
        <option value="' . $key . '" >' . $category_name . '</option>';
        }
      }
      $content .= '
      </select>';
    }
    if ($params->name_search) {
      if (isset($_POST['name_search'])) {
        $name_search = check_plain($_POST['name_search']);
      }
      else {
        $name_search = '';
      }
      $content .= '
      <br /><br />
      <span style="font-size:14px !important">' . t('Search by name') . '</span>&nbsp;
      <input type="text" id="name_search" name="name_search" class="spidercontactinput" value="' . $name_search . '">
      <input type="submit" value="' . t('Search') . '" class="spidercontactbutton" style="background-color:#' . $theme->cube_button_background_color . '; color:#' . $theme->cube_button_color . '; width:inherit;">
      <input type="button" value="' . t('Reset') . '" onClick="spider_contacts_cat_form_reset(this.form);" class="spidercontactbutton" style="background-color:#' . $theme->cube_button_background_color . '; color:#' . $theme->cube_button_color . '; width:inherit;">';
    }
    $content .= '
    </div>
  </form>';
  }
  $content .= '
  <table border="0" cellpadding="0" cellspacing="0" id="contactMainTable" style="width:inherit;">
    <tr>';
  foreach ($rows as $row) {
    $row_param = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $row))->fetchObject();
    if ((($prod_iterator % $theme->cube_count_of_contact_in_the_row) === 0) && ($prod_iterator > 0)) {
      $content .= '
    </tr>
    <tr>';
    }
    $prod_iterator++;
    $link = $permalink_for_sp_cat . '&contact_id=' . $row_param->id . '&page_num=' . $page_num . '&back=' . $nodeid;
    $imgurl = explode('#***#', $row_param->image_url);
    $content .= '
      <td>
        <div id="contactMainDivCube" style="' . ' border-width:' . $theme->cube_border_width . 'px;border-color:#' . $theme->cube_border_color . ';border-style:' . $theme->cube_border_style . ';' . (($theme->cube_text_size_small != '') ? ('font-size:' . $theme->cube_text_size_small . 'px;') : '') . (($theme->cube_text_color != '') ? ('color:#' . $theme->cube_text_color . ';') : '') . (($theme->cube_background_color != '') ? ('background-color:#' . $theme->cube_background_color . ';') : '') . ' width:' . $theme->cube_contact_cell_width . 'px; height:' . $theme->cube_contact_cell_height . 'px; ">
          <div style="height:' . ((int)$theme->cube_contact_cell_height - 20) . 'px;">
            <div id="contTitle" style="font-size:' . $theme->cube_title_size_small . 'px;' . (($theme->cube_title_color != '') ? ('color:#' . $theme->cube_title_color . ';') : '') . (($theme->cube_title_background_color != '') ? ('background-color:#' . $theme->cube_title_background_color . ';') : '') . '">
              <a style="padding: 5px;' . (($theme->cube_title_color != '') ? ('color:#' . $theme->cube_title_color . ';') : '') . '" href="' . $link . '">' . $row_param->first_name . ' ' . $row_param->last_name . '</a>
            </div>
            <table id="contMiddle" border="0" cellspacing="0" cellpadding="0">
              <tr>';
    if (!($row_param->image_url != '')) {
      $imgurl[0] = base_path() . drupal_get_path('module', 'spider_contacts') . '/images/no_image.jpg';
      $content .= '
                <td style="padding:10px;">
                  <img style="max-width:' . $theme->cube_small_picture_width . 'px; max-height:' . $theme->cube_small_picture_height . 'px" src="' . $imgurl[0] . '" />
                </td>';
    }
    else {
      $content .= '
                <td style="padding:10px;">
                  <a href="' . $imgurl[0] . '" target="_blank">
                    <img style="max-width:' . $theme->cube_small_picture_width . 'px;max-height:' . $theme->cube_small_picture_height . 'px" src="' . $imgurl[0] . '"/>
                  </a>
                </td>';
    }
    $content .= '
              </tr>
            </table>
          </div>
          <div style="float:right;" id="contMore">
            <a href="' . $link . '" style="' . (($theme->cube_hyperlink_color != '') ? ('color:#' . $theme->cube_hyperlink_color . ';') : '') . '">' . t('More') . '</a>
          </div>
        </div>
      </td>';
  }
  $content .= '
    </tr>
  </table>
  <div id="spidercontactnavigation" style="text-align:center;">';
  if ($cat_id != 0) {
    $page_link .= "&cat_id=" . $cat_id;
  }
  if ($name_search != "") {
    $page_link .= "&name_search=" . $name_search;
  }
  if (($prod_count > $prod_in_page) && ($prod_in_page > 0)) {
    $r = ceil($prod_count / $prod_in_page);
    $navstyle = (($theme->cube_text_size_small != '') ? ('font-size:' . $theme->cube_text_size_small . 'px !important;') : 'font-size:12px !important;') . (($theme->cube_text_color != '') ? ('color:#' . $theme->cube_text_color . ' !important;') : '');
    $link = $page_link . '&page_num= ';
    if ($page_num > 5) {
      $link = $page_link . '&page_num=1';
      $content .= '
        &nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('First') . '</a>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;...&nbsp;';
    }
    if ($page_num > 1) {
      $link = $page_link . '&page_num=' . ($page_num - 1);
      $content .= '
        &nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Prev') . '</a>&nbsp;&nbsp;';
    }
    for ($i = $page_num - 4; $i < ($page_num + 5); $i++) {
      if ($i <= $r and $i >= 1) {
        $link = $page_link . '&page_num=' . $i;
        if ($i == $page_num) {
          $content .= '
        <span style="font-weight:bold;color:#000000">&nbsp;' . $i . '&nbsp;</span>';
        }
        else {
          $content .= '
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">&nbsp;' . $i . '&nbsp;</a>';
        }
      }
    }
    if ($page_num < $r) {
      $link = $page_link . '&page_num=' . ($page_num + 1);
      $content .= '
        &nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Next') . '</a>&nbsp;&nbsp;';
    }
    if (($r - $page_num) > 4) {
      $link = $page_link . '&page_num=' . $r;
      $content .= '
        &nbsp;...&nbsp;&nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Last') . '</a>';
    }
  }
  $content .= '
  </div>';
  drupal_add_js('var SpiderCatOFOnLoad = window.onload; window.onload = SpiderCatAddToOnload;', array('type' => 'inline', 'scope' => 'footer'));
  return $content;
}
