<?php
/**
 * @file
 * Spider Contacts module contacts categories.
 */

/**
 * Menu loader callback. Load categories table.
 */
function spider_contacts_categories() {
  $form = array();
  $free_version = '<a href="http://web-dorado.com/drupal-contacts-guide-step-2.html" target="_blank" style="float:right;"><img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/user-manual.png" border="0" alt="' . t('User Manual') . '"></a><div style="clear:both;"></div>
  <a href="http://web-dorado.com/products/drupal-contacts-module.html" target="_blank" style="color:red; text-decoration:none; float:right;">
                    <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/header.png" border="0" alt="www.web-dorado.com" width="215"><br />
                  <div style="float:right;">' . t('Get the full version') . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
                  </a>';
  $form['fieldset_category_buttons'] = array(
    '#prefix' => $free_version,
    '#type' => 'fieldset',
  );
  $form['fieldset_category_buttons']['publish_category'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_categories_publish'),
    '#value' => t('Publish'),
  );
  $form['fieldset_category_buttons']['unpublish_category'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_categories_unpublish'),
    '#value' => t('Unpublish'),
  );
  $form['fieldset_category_buttons']['delete_category'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_categories_delete'),
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'if (!confirm(Drupal.t("Do you want to delete selected categories?"))) {return false;}'),
  );
  $form['fieldset_category_buttons']['save_order'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_save_ordering'),
    '#value' => t('Save order'),
  );
  $form['fieldset_category_buttons']['new_category'] = array(
    '#prefix' => l(t('New'), url('admin/settings/spider_contacts/categories/edit', array('absolute' => TRUE))),
  );
  $form['fieldset_search_categories_name'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search categories by name'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_contacts_search_categories_name', '') == '') ? TRUE : FALSE),
  );
  $form['fieldset_search_categories_name']['search_categories_name'] = array(
    '#type' => 'textfield',
    '#size' => 25,
    '#default_value' => variable_get('spider_contacts_search_categories_name', ''),
  );
  $form['fieldset_search_categories_name']['search_categories'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_search_categories_name'),
    '#value' => t('Go'),
  );
  $form['fieldset_search_categories_name']['reset_categories'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('spider_contacts_reset_categories_name'),
  );

  drupal_add_tabledrag('categories_table-components', 'order', 'sibling', 'weight');
  $header = array(
    'checkbox' => array('class' => 'select-all'),
    'id' => array('data' => t('ID'), 'field' => 'n.id'),
    'name' => array('data' => t('Name'), 'field' => 'n.name'),
    'description' => array('data' => t('Description'), 'field' => 'n.description'),
    'order' => array('data' => t('Order'), 'field' => 'n.ordering', 'sort' => 'asc'),
    'published' => array('data' => t('Published')),
    'delete' => array('data' => t('Delete')),
  );
  $options = array();
  $categories_ids = db_select('spider_contacts_contacts_categories', 'n')
    ->fields('n', array('id'))
    ->condition('n.name', '%' . db_like(variable_get('spider_contacts_search_categories_name', '')) . '%', 'LIKE')
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(20)
    ->execute()
    ->fetchCol();
  foreach ($categories_ids as $category_id) {
    $row = db_query("SELECT * FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $category_id))->fetchObject();
    if ($row->published) {
      $publish_unpublish_png = 'publish.png';
      $publish_unpublish_function = 'unpublish';
    }
    else {
      $publish_unpublish_png = 'unpublish.png';
      $publish_unpublish_function = 'publish';
    }
    variable_set('spider_contacts_category_id', $category_id);
    $options[$category_id] = array(
      'checkbox' => array(
        'data' => array(
          '#type' => 'checkbox',
          '#attributes' => array('name' => 'spider_contacts_category_check_' . $category_id),
        ),
      ),
      'id' => $category_id,
      'name' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $row->name,
          '#href' => url('admin/settings/spider_contacts/categories/edit', array('query' => array('category_id' => $category_id), 'absolute' => TRUE)),
        ),
      ),
      'description' => $row->description,
      'order' => array(
        'data' => array(
          '#type' => 'textfield',
          '#size' => 3,
          '#value' => $row->ordering,
          '#attributes' => array('name' => 'spider_contacts_categories_order_' . $category_id, 'class' => array('weight')),
        ),
      ),
      'published' => l(t('<img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/' . $publish_unpublish_png . '" />'), url('admin/settings/spider_contacts/categories/' . $publish_unpublish_function, array('query' => array('category_id' => $category_id), 'absolute' => TRUE)), array('html' => TRUE)),
      'delete' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('Delete'),
          '#href' => url('admin/settings/spider_contacts/categories/delete', array('query' => array('category_id' => $category_id), 'absolute' => TRUE)),
        ),
      ),
    );
    $options[$category_id]['#attributes'] = array('class' => array('draggable'));
  }
  $form['categories_table'] = array(
    '#type' => 'tableselect',
    '#js_select' => TRUE,
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No categories available.'),
    '#suffix' => theme('pager', array('tags' => array())),
    '#attributes' => array('id' => 'categories_table-components'),
  );
  foreach ($categories_ids as $category_id) {
    $form['categories_table'][$category_id]['#disabled'] = TRUE;
  }
  return $form;
}

/**
 * Search categories by name.
 */
function spider_contacts_search_categories_name($form, &$form_state) {
  if ($form_state['values']['search_categories_name'] != '') {
    variable_set('spider_contacts_search_categories_name', $form_state['values']['search_categories_name']);
  }
  else {
    variable_set('spider_contacts_search_categories_name', '');
  }
}

/**
 * Reset categories by name.
 */
function spider_contacts_reset_categories_name($form, &$form_state) {
  variable_set('spider_contacts_search_categories_name', '');
}

/**
 * Publish selected categories.
 */
function spider_contacts_categories_publish($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_contacts_categories}")) {
    $category_ids_col = db_query("SELECT id FROM {spider_contacts_contacts_categories}")->fetchCol();
    $flag = FALSE;
    foreach ($category_ids_col as $category_id) {
      if (isset($_POST['spider_contacts_category_check_' . $category_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_contacts_contacts_categories} SET published=:published WHERE id=:id", array(':published' => 1, ':id' => $category_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one category.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Categories successfully published.'), 'status', FALSE);
    }
  }
}

/**
 * Publish category.
 */
function spider_contacts_category_publish() {
  if (isset($_GET['category_id'])) {
    $category_id = check_plain($_GET['category_id']);
    db_query("UPDATE {spider_contacts_contacts_categories} SET published=:published WHERE id=:id", array(':published' => 1, ':id' => $category_id));
  }
  drupal_set_message(t('Category successfully published.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/categories', array('absolute' => TRUE)));
}

/**
 * Unpublish selected categories.
 */
function spider_contacts_categories_unpublish($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_contacts_categories}")) {
    $category_ids_col = db_query("SELECT id FROM {spider_contacts_contacts_categories}")->fetchCol();
    $flag = FALSE;
    foreach ($category_ids_col as $category_id) {
      if (isset($_POST['spider_contacts_category_check_' . $category_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_contacts_contacts_categories} SET published=:published WHERE id=:id", array(':published' => 0, ':id' => $category_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one category.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Categories successfully unpublished.'), 'status', FALSE);
    }
  }
}

/**
 * Unpublish category.
 */
function spider_contacts_category_unpublish() {
  if (isset($_GET['category_id'])) {
    $category_id = check_plain($_GET['category_id']);
    db_query("UPDATE {spider_contacts_contacts_categories} SET published=:published WHERE id=:id", array(':published' => 0, ':id' => $category_id));
  }
  drupal_set_message(t('Category successfully unpublished.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/categories', array('absolute' => TRUE)));
}

/**
 * Delete selected categories.
 */
function spider_contacts_categories_delete($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_contacts_categories}")) {
    $category_ids_col = db_query("SELECT id FROM {spider_contacts_contacts_categories}")->fetchCol();
    $flag = FALSE;
    foreach ($category_ids_col as $category_id) {
      if (isset($_POST['spider_contacts_category_check_' . $category_id])) {
        $flag = TRUE;
        db_query("DELETE FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $category_id));
        // Set deleted category contact category uncategorized.
        db_query("UPDATE {spider_contacts_contacts} SET category_id=0 WHERE category_id=" . $category_id);
        drupal_set_message(t('Selected categories successfully deleted.'), 'status', FALSE);
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one category.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Categories successfully deleted.'), 'status', FALSE);
    }
  }
}

/**
 * Delete category.
 */
function spider_contacts_category_delete() {
  if (isset($_GET['category_id'])) {
    $category_id = check_plain($_GET['category_id']);
    db_query("DELETE FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $category_id));
    // Set deleted category contact category uncategorized.
    db_query("UPDATE {spider_contacts_contacts} SET category_id=0 WHERE category_id=" . $category_id);
  }
  drupal_set_message(t('Category successfully deleted.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/categories', array('absolute' => TRUE)));
}

/**
 * Save ordering.
 */
function spider_contacts_save_ordering($form, &$form_state) {
  $categories_ids = db_query("SELECT id FROM {spider_contacts_contacts_categories}")->fetchCol();
  foreach ($categories_ids as $category_id) {
    if (isset($_POST['spider_contacts_categories_order_' . $category_id])) {
      $ordering = check_plain($_POST['spider_contacts_categories_order_' . $category_id]);
      db_query("UPDATE {spider_contacts_contacts_categories} SET ordering=:ordering WHERE id=:id", array(':ordering' => $ordering, ':id' => $category_id));
    }
    drupal_set_message(t('Category ordering successfully saved.'), 'status', FALSE);
  }
}

/**
 * Add or edit category.
 */
function spider_contacts_category_edit() {
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_params.js');
  drupal_add_js(array(
    'spider_contacts' => array(
      'delete_png_url' => base_path() . drupal_get_path('module', 'spider_contacts') . '/images/',
    ),
    ),
    'setting');
  if (isset($_GET['category_id'])) {
    $category_id = check_plain($_GET['category_id']);
    $row = db_query("SELECT * FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $category_id))->fetchObject();
    $category_name = $row->name;
    $category_description = $row->description;
    $category_param = $row->param;
    $category_published = $row->published;
  }
  else {
    $category_id = '';
    $category_name = '';
    $category_description = '';
    $category_param = '';
    $category_published = 1;
  }
  if (file_exists("sites/all/libraries/tinymce/jscripts/tiny_mce/tiny_mce.js")) {
    drupal_add_js('sites/all/libraries/tinymce/jscripts/tiny_mce/tiny_mce.js');
    drupal_add_js('tinyMCE.init({
				// General options
        mode : "specific_textareas",
        editor_selector : "spider_contacts_editor",
				theme : "advanced",
				plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
				// Theme options
				theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,

				// Skin options
				skin : "o2k7",
				skin_variant : "silver",

				// Example content CSS (should be your site CSS)
				//content_css : "css/example.css",
				
				// Drop lists for link/image/media/template dialogs
				template_external_list_url : "js/template_list.js",
				external_link_list_url : "js/link_list.js",
				external_image_list_url : "js/image_list.js",
				media_external_list_url : "js/media_list.js",

				// Replace values for the template plugin
				template_replace_values : {
					username : "Some User",
					staffid : "991234"
				}
			});', array('type' => 'inline', 'scope' => 'footer'));
  }
  $form = array();
  $form['category_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $category_name,
    '#size' => 15,
  );
  $message_for_without_editor = '
    <div class="messages error" style="width:650px;">
      ' . t('To show HTML editor download "tinymce" library from !url and extract it into "sites/all/libraries/tinymce" directory.', array('!url' => l(t('here'), 'http://github.com/downloads/tinymce/tinymce/tinymce_3.5.7.zip'))) . '
    </div>';
  $form['category_description'] = array(
    '#prefix' => (file_exists("sites/all/libraries/tinymce/jscripts/tiny_mce/tiny_mce.js") ? '' : $message_for_without_editor) . '<div style="width:650px;">',
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $category_description,
    '#attributes' => array('class' => array('spider_contacts_editor')),
    '#resizable' => FALSE,
    '#suffix' => '</div>',
  );
  drupal_add_js('
    var category_parameters_array = "' . $category_param . '";
    var parameters0 = new Array();
    parameters0["sel1"] = category_parameters_array.split("#***#");
    /*parameters0["sel1"].push("");*/', array('type' => 'inline'));
  $form['category_hidden'] = array(
    '#type' => 'hidden',
    '#value' => $category_param,
  );
  $par = explode("#***#", $category_param);
  $k = 0;
  $param_items = '<strong>' . t('Parameters') . '</strong><div id="sel1">';
  while ($k < 1000) {
    if (isset($par[$k]) && $par[$k] != '') {
      $param_items .= '
        <input type="text" class="form-text" style="width:200px;" id="inp_sel1_' . $k . '" value="' . $par[$k] . '" onChange=\'spider_contacts_add("sel1")\' />
        <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/delete.png" style="cursor:pointer;" onclick=\'spider_contacts_remove(' . $k . ', "sel1");\'><br />';
      $k++;
    }
    else {
      $param_items .= '
        <input type="text" class="form-text" style="width:200px;" id="inp_sel1_' . $k . '" value="" onChange=\'spider_contacts_add("sel1")\' />
        <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/delete.png" style="cursor:pointer;" onclick=\'spider_contacts_remove(' . $k . ', "sel1");\'><br />';
      $k = 1000;
    }
  }
  $param_items .= '</div>
                   <input type="hidden" name="param" id="hid_sel1" value="' . $category_param . '" />
                   <div class="description">' . t('Enter parameter name.') . '</div>';
  $form['category_published'] = array(
    '#prefix' => $param_items,
    '#type' => 'radios',
    '#title' => t('Published'),
    '#default_value' => $category_published,
    '#options' => array('1' => t('Yes'), '0' => t('No')),
  );
  $form['category_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('spider_contacts_category_save'),
  );
  $form['category_apply'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#submit' => array('spider_contacts_category_apply'),
  );
  $form['category_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#attributes' => array('onclick' => 'document.getElementById("edit-category-name").setAttribute("style", "color:rgba(255, 0, 0, 0)");document.getElementById("edit-category-name").setAttribute("value", "cancel");'),
    '#submit' => array('spider_contacts_category_cancel'),
  );
  return $form;
}

/**
 * Save category.
 */
function spider_contacts_category_save($form, &$form_state) {
  $category_name = $form_state['values']['category_name'];
  $category_description = $form_state['values']['category_description'];
  if (isset($_POST['param'])) {
    $parameters_array = check_plain($_POST['param']);
  }
  else {
    $parameters_array = '';
  }
  $category_published = $form_state['values']['category_published'];
  if (isset($_GET['category_id'])) {
    $category_id = check_plain($_GET['category_id']);
    $parameters = explode('#***#', $parameters_array);
    $cat_old_params = explode('#***#', $form_state['values']['category_hidden']);
    $params = db_query("SELECT param FROM {spider_contacts_contacts} WHERE category_id=:category_id", array(':category_id' => $category_id))->fetchCol();
    foreach ($params as $param) {
      $new_param = $param;
      foreach ($cat_old_params as $key => $cat_old_param) {
        if ($cat_old_param != '') {
          $new_param = str_replace($cat_old_param . '@@:@@', $parameters[$key] . '@@:@@', $new_param);
        }
      }
      db_query("UPDATE {spider_contacts_contacts} SET param='" . $new_param . "' WHERE param='" . $param . "'");
    }
    db_query("UPDATE {spider_contacts_contacts_categories} SET 
      name=:name,
      description=:description,
      param=:param,
      published=:published WHERE id=:id", array(
      ':name' => $category_name,
      ':description' => $category_description,
      ':param' => $parameters_array,
      ':published' => $category_published,
      ':id' => $category_id));
  }
  else {
    db_insert('spider_contacts_contacts_categories')
      ->fields(array(
        'name' => $category_name,
        'description' => $category_description,
        'param' => $parameters_array,
        'ordering' => 0,
        'published' => $category_published,
        ))
      ->execute();
  }
  drupal_set_message(t('Your category successfully saved.'), 'status', FALSE);
  $form_state['redirect'] = url('admin/settings/spider_contacts/categories', array('absolute' => TRUE));
}

/**
 * Apply category.
 */
function spider_contacts_category_apply($form, &$form_state) {
  $category_name = $form_state['values']['category_name'];
  $category_description = $form_state['values']['category_description'];
  if (isset($_POST['param'])) {
    $parameters_array = check_plain($_POST['param']);
  }
  else {
    $parameters_array = '';
  }
  $category_published = $form_state['values']['category_published'];
  if (isset($_GET['category_id'])) {
    $category_id = check_plain($_GET['category_id']);
    $parameters = explode('#***#', $parameters_array);
    $cat_old_params = explode('#***#', $form_state['values']['category_hidden']);
    $params = db_query("SELECT param FROM {spider_contacts_contacts} WHERE category_id=:category_id", array(':category_id' => $category_id))->fetchCol();
    foreach ($params as $param) {
      $new_param = $param;
      foreach ($cat_old_params as $key => $cat_old_param) {
        if ($cat_old_param != '') {
          $new_param = str_replace($cat_old_param . '@@:@@', $parameters[$key] . '@@:@@', $new_param);
        }
      }
      db_query("UPDATE {spider_contacts_contacts} SET param='" . $new_param . "' WHERE param='" . $param . "'");
    }
    db_query("UPDATE {spider_contacts_contacts_categories} SET 
      name=:name,
      description=:description,
      param=:param,
      published=:published WHERE id=:id", array(
      ':name' => $category_name,
      ':description' => $category_description,
      ':param' => $parameters_array,
      ':published' => $category_published,
      ':id' => $category_id));
    drupal_set_message(t('Your category successfully updated.'), 'status', FALSE);
  }
  else {
    db_insert('spider_contacts_contacts_categories')
      ->fields(array(
        'name' => $category_name,
        'description' => $category_description,
        'param' => $parameters_array,
        'ordering' => 0,
        'published' => $category_published,
        ))
      ->execute();
    $category_id = db_query("SELECT MAX(id) FROM {spider_contacts_contacts_categories}")->fetchField();
    drupal_set_message(t('Your category successfully saved.'), 'status', FALSE);
  }
  $form_state['redirect'] = url('admin/settings/spider_contacts/categories/edit', array('query' => array('category_id' => $category_id), 'absolute' => TRUE));
}

/**
 * Cancel category save.
 */
function spider_contacts_category_cancel($form, &$form_state) {
  $form_state['redirect'] = url('admin/settings/spider_contacts/categories', array('absolute' => TRUE));
}
