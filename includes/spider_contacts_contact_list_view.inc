<?php
/**
 * @file
 * This file contains all functions which need for calendar view.
 */
/**
 * View products in list.
 */
function spider_contacts_contact_list($nodeid, $theme_id, $category_id) {
  // Parameters for SpiderBox.
  drupal_add_js(array(
    'spider_contacts' => array(
      'delay' => 3000,
      'allImagesQ' => 0,
      'slideShowQ' => 0,
      'darkBG' => 1,
      'juriroot' => base_path() . drupal_get_path('module', 'spider_contacts'),
      'spiderShop' => 1,
    ),
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/spiderBox/spiderBox.js');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_common.js');
  drupal_add_css(drupal_get_path('module', 'spider_contacts') . '/css/spider_contacts_main.css');
  if (isset($_GET['page_num'])) {
    $page_num = $_GET['page_num'];
  }
  else {
    $page_num = 1;
  }
  if (isset($_POST['name_search'])) {
    $name_search = check_plain($_POST['name_search']);
  }
  else {
    $name_search = '';
  }
  if (isset($_POST['cat_id'])) {
    $cat_id = check_plain($_POST['cat_id']);
  }
  else {
    $cat_id = $category_id;
  }
  // If category has deleted, show all contacts.
  if (!db_query("SELECT id FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $cat_id))->fetchField()) {
    $cat_id = 0;
  }
  if ($cat_id == 0) {
    $compare = '>=';
  }
  else {
    $compare = '=';
  }
  $page_link = url('node/' . $nodeid, array('query' => array('example' => 1), 'absolute' => TRUE));
  $content = '';
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  if (!$theme) {
    $theme_id = 1;
    db_query("UPDATE {spider_contacts_form_table} SET theme=:theme WHERE vid=:vid", array(':theme' => 1, ':vid' => $nodeid));
  }
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  $params = db_query("SELECT * FROM {spider_contacts_params} WHERE id=:id", array(':id' => 1))->fetchObject();
  $category_list = db_query("SELECT id,name FROM {spider_contacts_contacts_categories} WHERE published=:published ORDER BY ordering", array(':published' => 1))->fetchAllKeyed();
  $prod_in_page = $theme->count_of_rows_in_the_table;
  $rows = db_query("SELECT id FROM {spider_contacts_contacts} WHERE published=:published AND category_id" . $compare . ":category_id AND (first_name LIKE :first_name OR last_name LIKE :last_name) ORDER BY ordering LIMIT " . (($page_num - 1) * $prod_in_page) . ", " . $prod_in_page, array(':published' => 1, ':category_id' => $cat_id, ':first_name' => '%' . db_like($name_search) . '%', ':last_name' => '%' . db_like($name_search) . '%'))->fetchCol();
  $prod_count = db_query("SELECT COUNT(id) FROM {spider_contacts_contacts} WHERE published=1 AND category_id" . $compare . ":category_id AND (first_name LIKE :first_name OR last_name LIKE :last_name)", array(':category_id' => $cat_id, ':first_name' => '%' . db_like($name_search) . '%', ':last_name' => '%' . db_like($name_search) . '%'))->fetchField();
  $permalink_for_sp_cat = url('spider_contacts/contact_view', array(
    'query' => array('theme_id' => $theme_id),
    'absolute' => TRUE
  ));
  drupal_add_js('
    function spider_contacts_submit_catal(page_link) {
      if (document.getElementById("cat_form")) {
          document.getElementById("cat_form").setAttribute("action", page_link);
          document.getElementById("cat_form").submit();
        }
        else {
          window.location.href = page_link;
        }
    }', array('type' => 'inline'));
  if ($theme->table_radius) {
    $border_radius = '8px';
  }
  else {
    $border_radius = '0';
  }
  $content .= '
    <style type="text/css">
      #table_contact th {
         background: none repeat scroll 0 0 transparent;
      }
      .spidercontactparamslist {
        margin: 0 !important;
        padding: 0 !important;
      }
      #tdviewportheight {
        border: none;
      }
      #contactMainDiv,
      .spidercontactbutton,
      .spidercontactinput,
      .table_contact {
        -webkit-border-radius: ' . $border_radius . ';
        -moz-border-radius: ' . $border_radius . ';
        border-radius: ' . $border_radius . ';
      }
      #contactssMainDivs th {
        text-transform:none !important;
        text-align:center !important;
      }
      #contactMainDiv #contTitle {
        -webkit-border-top-right-radius: ' . $border_radius . ';
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        -moz-border-radius-topleft: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
      }
      select.input {
        margin: 0 0 24px 0 !important;
        top: -11px !important;
        position: relative;
      }
      #ContactSearchBox,
      .spidercontactbutton {
        font-size:10px !important;
        -webkit-border-radius: ' . $border_radius . ' !important;
        -moz-border-radius: ' . $border_radius . ' !important;
        border-radius: ' . $border_radius . ' !important;
        padding: 4px;        
      }
      .input {
        text-align: left !important;
        font-size: 10px !important;
      }
      #ContactSearchBox {
        margin-bottom:10px !important;
        text-align: right !important;
      }
      #contactssMainDivs,
      #table_contact,
      .spidercontactbutton {
        -webkit-border-radius: ' . $border_radius . ';
        -moz-border-radius: ' . $border_radius . ';
        border-radius: ' . $border_radius . ';
      }
      #contactssMainDivs table,
      #contactssMainDivs td,
      #contactssMainDivs tr,
      #contactssMainDivs div,
      #contactssMainDivs tbody,
      #contactssMainDivs th {
        line-height:inherit;
      }
      #contactssMainDivs tr, td {
        padding:inherit !important;
      }
      #contactssMainDivs img {
        max-width:inherit;
        max-height:inherit;
      }
      #contactssMainDivs ul li,
      #contactssMainDivs ul,
      #contactssMainDivs li {
        list-style-type:none !important;
      }
      #contactssMainDivs #prodTitle {
        -webkit-border-top-right-radius: ' . $border_radius . ';
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        -moz-border-radius-topleft: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
      }
      #table_contact tr:first-child td:first-child {
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -moz-border-radius-topleft: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
      }
      #table_contact {
        margin:inherit;
      }
      #table_contact table,
      #table_contact tr,
      #table_contact td {
        padding-bottom:0px;
        padding-left:0px;
        padding-right:0px;
        padding-top:0px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:0px;
      }
      #table_contact{
        padding-bottom:0px;
        padding-left:0px;
        padding-right:0px;
        padding-top:0px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:0px;
      }
      #table_contact tr:first-child td:last-child {
        -webkit-border-top-right-radius: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
      }
      #table_contact tr:last-child td:first-child {
        -webkit-border-bottom-left-radius: ' . $border_radius . ';
        -moz-border-radius-bottomleft: ' . $border_radius . ';
      }
      #table_contact tr:last-child td:last-child {
        -webkit-border-bottom-right-radius: ' . $border_radius . ';
        -moz-border-radius-bottomright: ' . $border_radius . ';
      }
      #table_contact td table td {
        -webkit-border-radius: 0px !important;
        -moz-border-radius: 0px !important;
        border-radius: 0px !important;
      }
      #contactssMainDivs td,
      #contactssMainDivs tr,
      #contactssMainDivs tbody,
      #contactssMainDivs div {
        line-height:inherit !important;
        color:inherit;
        opacity:inherit !important;
      }
      #category,
      #category table,
      #category tr,
      #category td,
      #category th,
      #category tbody {
        margin-left:0px;
        margin-bottom:0px;
        margin-right:0px;
        margin-top:0px;
        padding-bottom: 10px;
        padding-left: 10px;
        padding-right: 10px;
      }
      #table_contact ul,
      #table_contact li,
      #table_contact a {
        background-color:inherit;
        list-style:none;
      }
      #table_of_param tr,
      #table_of_param td {
        text-align:left;
        vertical-align:top !important;
      }
      #spider_cat_price_tab {
        vertical-align:top !important;
      }
      #contactssMainDivs input,
      #contactssMainDivs textarea {
        -webkit-transition: all 0.2s linear;
        -webkit-transition-delay: 0s;
        -moz-transition: all 0.2s linear 0s;
        -o-transition: all 0.2s linear 0s;
        transition: all 0.2s linear 0s;
      }
      #contactssMainDivs textarea,
      #contactssMainDivs input[type="text"],
      #contactssMainDivs select {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-background-clip: padding;
        -moz-background-clip: padding;
        background-clip: padding-box;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-appearance: none;
        background-color: white;
        border: 1px solid #cccccc;
        color: black;
        outline: 0;
        margin: 0;
        padding: 3px 4px;
        text-align: left;
        font-size: 13px;
        font-family: Arial, "Liberation Sans", FreeSans, sans-serif;
        vertical-align: top;
        *padding-top: 2px;
        *padding-bottom: 1px;
        *height: auto;
      }
      #contactssMainDivs textarea[disabled],
      #contactssMainDivs input[type="text"][disabled] {
        background-color: #eeeeee;
      }
      #contactssMainDivs textarea:focus,
      #contactssMainDivs input[type="text"]:focus {
        border-color: rgba(82, 168, 236, 0.8);
        outline: 0;
        outline: thin dotted \9;
        /* IE6-9 */
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(82, 168, 236, 0.6);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(82, 168, 236, 0.6);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(82, 168, 236, 0.6);
      }
      #contactssMainDivs input[disabled],
      #contactssMainDivs textarea[disabled] {
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        -moz-user-select: -moz-none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        user-select: none;
        color: #888888;
        cursor: default;
      }
      #contactssMainDivs input::-webkit-input-placeholder,
      #contactssMainDivs textarea::-webkit-input-placeholder {
        color: #888888;
      }
      #contactssMainDivs input:-moz-placeholder,
      #contactssMainDivs textarea:-moz-placeholder {
        color: #888888;
      }
      #contactssMainDivs input.placeholder_text,
      #contactssMainDivs textarea.placeholder_text {
        color: #888888;
      }
      #contactssMainDivs optgroup {
        color: black;
        font-style: normal;
        font-weight: normal;
        font-family: Arial, "Liberation Sans", FreeSans, sans-serif;
      }
      #contactssMainDivs optgroup::-moz-focus-inner {
        border: 0;
        padding: 0;
      }
      #tdviewportheight div,
      #tdviewportheight td,
      #tdviewportheight {
        vertical-align:middle;
      }
      #table_contact tr,
      #table_contact td,
      #table_contact tr td {
        border:1px solid ;
      }
      #table_of_param td,
      #table_of_param tr,
      #table_of_param table {
        border:1px solid !important;
        border-color:#FFF !important;
        border-spacing:inherit !important;
      }
      #table_contact td,
      #table_contact th,
      #table_contact,
      #table_contact span,
      #table_contact div {
        overflow:hidden;
      }
      #table_contact tr {
        display: table-header-group;
      }
      #table_contact {
        display:block;
        overflow:auto;
      }
      #table_contact td {
        vertical-align:middle !important;
        text-align:center !important;
      }
      #contact_email_div {
        word-break:break-all !important;
      }
    </style>';
  if ($theme->change_on_hover) {
    $content .= '
    <style>
      .table_contact tr:hover {
        background-color:#' . $theme->hover_color . ';
      }
      .table_contact tr:hover #contTitle,
      .table_contact tr:hover span,
      .table_contact tr:hover a {
        background-color:#' . $theme->hover_color . ';
        color:#' . $theme->hover_text_color . ' !important;
      }
    </style>';
  }
  $a = 'form';
  if ($params->choose_category || $params->name_search) {
    $content .= '
    <div id="contactssMainDivs">
      <' . $a . ' action="' . $page_link . '" method="post" name="cat_form" id="cat_form" style="text-align:right;">
      <input type="hidden" name="page_num" value="1">
      <div class="ContactSearchBox">';
    if ($params->choose_category && ($category_id == 0)) {
      $content .= '
          <span style="font-size:14px !important">' . t('Choose Category') . '</span>&nbsp;
          <select id="cat_id" name="cat_id" class="spidercontactinput" size="1" onChange="document.cat_form.submit();">
            <option value="0">' . t('All') . '</option> ';
      foreach ($category_list as $key => $category_name) {
        if ($key == $cat_id) {
          $content .= '
            <option value="' . $key . '"  selected="selected">' . $category_name . '</option>';
        }
        else {
          $content .= '
            <option value="' . $key . '" >' . $category_name . '</option>';
        }
      }
      $content .= '
          </select>';
    }
    if ($params->name_search) {
      if (isset($_POST['name_search'])) {
        $name_search = check_plain($_POST['name_search']);
      }
      else {
        $name_search = '';
      }
      $content .= '
          <br /><br />
          <span style="font-size:14px !important">' . t('Search by name') . '</span>&nbsp;
          <input id="name_search" name="name_search" class="input" value="' . $name_search . '">
          <input type="submit" value="' . t('Search') . '" class="spidercontactbutton" style="background-color:#' . $theme->table_button_background_color . '; color:#' . $theme->table_button_color . '; width:inherit;">
          <input type="button" value="' . t('Reset') . '" onClick="spider_contacts_cat_form_reset(this.form);" class="spidercontactbutton" style="background-color:#' . $theme->table_button_background_color . '; color:#' . $theme->table_button_color . '; width:inherit;">';
    }
    $content .= '
        </div>
      </form>';
  }
  $content .= '
      <table class="table_contact" border="12px" style="border:' . $theme->table_border_style . ' ' . $theme->table_border_width . 'px !important; border-collapse:separate;  ' . 'border-color:#' . $theme->table_border_color . ' !important ;border-style:' . $theme->table_border_style . '; ' . (($theme->table_text_size_small != '') ? ('font-size:' . $theme->table_text_size_small . 'px;') : '') . (($theme->table_text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '') . (($theme->table_background_color != '') ? ('background-color:#' . $theme->table_background_color . ';') : '') . '" cellpadding="0" cellspacing="0" id="table_contact">';
  if (count($rows)) {
    $content .= '
        <tr style="' . (($theme->table_title_background_color != '') ? ('background-color:#' . $theme->table_title_background_color . ';') : '') . '" align="center">
          <th style="padding:5px; border-color:#' . $theme->table_border_color . ' !important ;' . (($theme->table_title_color != '') ? ('color:#' . $theme->table_title_color . ';') : ' ') . ' ' . (($theme->table_title_size_small != '') ? ('font-size:' . $theme->table_title_size_small . 'px;') : ' ') . '">' . t('Picture') . '</th>
          <th style="padding:5px; border-color:#' . $theme->table_border_color . ' !important ;' . (($theme->table_title_color != '') ? ('color:#' . $theme->table_title_color . ';') : ' ') . ' ' . (($theme->table_title_size_small != '') ? ('font-size:' . $theme->table_title_size_small . 'px;') : ' ') . '">' . t('First Name') . '</th>
          <th style="padding:5px; border-color:#' . $theme->table_border_color . ' !important ;' . (($theme->table_title_color != '') ? ('color:#' . $theme->table_title_color . ';') : ' ') . ' ' . (($theme->table_title_size_small != '') ? ('font-size:' . $theme->table_title_size_small . 'px;') : ' ') . '">' . t('Last Name') . '</th>
          <th style="padding:5px; border-color:#' . $theme->table_border_color . ' !important ;' . (($theme->table_title_color != '') ? ('color:#' . $theme->table_title_color . ';') : ' ') . ' ' . (($theme->table_title_size_small != '') ? ('font-size:' . $theme->table_title_size_small . 'px;') : ' ') . '">' . t('Category') . '</th>
          <th style="padding:5px; border-color:#' . $theme->table_border_color . ' !important ;' . (($theme->table_title_color != '') ? ('color:#' . $theme->table_title_color . ';') : ' ') . ' ' . (($theme->table_title_size_small != '') ? ('font-size:' . $theme->table_title_size_small . 'px;') : ' ') . '">' . t('Email') . '</th>
          <th style="padding:5px; width:' . ($theme->table_parameters_select_box_width - 10) . 'px; border-color:#' . $theme->table_border_color . ' !important ; ' . (($theme->table_title_color != '') ? ('color:#' . $theme->table_title_color . ';') : ' ') . ' ' . (($theme->table_title_size_small != '') ? ('font-size:' . $theme->table_title_size_small . 'px;') : ' ') . '">' . t('Parameters') . '</th>
        </tr>';
    foreach ($rows as $row) {
      $row_param = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $row))->fetchObject();
      $imgurl = explode("#***#", $row_param->image_url);
      $content .= '
        <tr style="border-color:#' . $theme->table_border_color . ' !important;" align="center">
          <td style="border-color:#' . $theme->table_border_color . ' !important;" id="picture">';
      if (!($row_param->image_url != '')) {
        $imgurl[0] = base_path() . drupal_get_path('module', 'spider_contacts') . '/images/no_image.jpg';
        $content .= '
            <div style="margin:5px;">
              <a href="' . $imgurl[0] . '" target="_self">
                <img style="max-width:' . $theme->table_small_picture_width . 'px;max-height:' . $theme->table_small_picture_height . 'px" src="' . $imgurl[0] . '" />
              </a>
            </div>';
      }
      else {
        $content .= '
            <div style="margin:5px;">
              <a href="' . $imgurl[0] . '" target="_self">
                <img style="max-width:' . $theme->table_small_picture_width . 'px ; max-height:' . $theme->table_small_picture_height . 'px" src="' . $imgurl[0] . '" />
              </a>
            </div>';
      }
      $link = $permalink_for_sp_cat . '&contact_id=' . $row_param->id . '&page_num=' . $page_num . '&back=' . $nodeid;
      $content .= '
          </td>
          <td style="border-color:#' . $theme->table_border_color . ' !important;" id="first_name">
            <div id="contTitle" style="margin:5px;font-size:' . $theme->table_title_size_small . 'px;' . (($theme->table_text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '') . '">
              <a style="padding: 5px;' . (($theme->table_text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '') . '" href="' . $link . '">' . $row_param->first_name . '</a>
            </div>
          </td>
          <td style="border-color:#' . $theme->table_border_color . ' !important;" id="last_name">
            <div id="contTitle" style="margin:5px; font-size:' . $theme->table_title_size_small . 'px;' . (($theme->table_text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '') . '">
              <a style="padding: 5px;' . (($theme->table_text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '') . '" href="' . $link . '">' . $row_param->last_name . '</a>
            </div>
          </td>
          <td style="border-color:#' . $theme->table_border_color . ' !important;" id="category">';
      if ($row_param->category_id == 0) {
        $category_title = t('Uncategorized');
      }
      else {
        $category_title = $category_list[$row_param->category_id];
      }
      $content .= '
            <div style="margin:5px;' . (($theme->table_text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '') . '">
              <span id="cat_' . $row_param->id . '">' . $category_title . '</span>
            </div>
          </td>
          <td style="border-color:#' . $theme->table_border_color . ' !important;" id="category">';
      if ($row_param->email != '') {
        $content .= '
            <div id="contact_email_div" margin:5px;' . (($theme->table_text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '') . '">
              <span id="email_' . $row_param->id . '">
                <a href="mailto:' . $row_param->email . '">' . $row_param->email . '</a>
              </span>
            </div>';
      }
      else {
        $content .= '
            <span id="email_' . $row_param->id . '"></span>';
      }
      $content .= '
          </td>
          <td align="justify" style="border-color:#' . $theme->table_border_color . ' !important;padding-top:0;padding:0;" id="params">
            <table id="table_of_param" style="border-collapse:separate;width:' . $theme->table_parameters_select_box_width . 'px;">';
      $par_data = explode("#@@@#", $row_param->param);
      for ($j = 0; $j < count($par_data); $j++) {
        if ($par_data[$j] != '') {
          $par1_data = explode("@@:@@", $par_data[$j]);
          $par_values = explode("#***#", $par1_data[1]);
          $countOfPar = 0;
          for ($k = 0; $k < count($par_values); $k++) {
            if ($par_values[$k] != "") {
              $countOfPar++;
            }
          }
          $bgcolor = ((!($j % 2)) ? (($theme->table_params_background_color2 != '') ? ('background-color:#' . $theme->table_params_background_color2 . ';') : '') : (($theme->table_params_background_color1 != '') ? ('background-color:#' . $theme->table_params_background_color1 . ';') : ''));
          if ($countOfPar != 0) {
            $paramscolor = ((!($j % 2)) ? (($theme->table_params_color2 != '') ? ('color:#' . $theme->table_params_color2 . ';') : '') : (($theme->table_params_color1 != '') ? ('color:#' . $theme->table_params_color1 . ';') : ''));
            $content .= '
              <tr style="' . $bgcolor . ' text-align:left">
                <td><b>' . $par1_data[0] . ':</b></td>
                <td id="nohover" style="' . (($theme->table_text_size_small != '') ? ('font-size:' . $theme->table_text_size_small . 'px;') : '') . $bgcolor . ' ' . $paramscolor . ' width:' . $theme->table_parameters_select_box_width . 'px;">
                  <ul class="spidercontactparamslist">';
            for ($k = 0; $k < count($par_values); $k++)
              if ($par_values[$k] != "") {
                if (preg_match('/(http:\/\/[^\s]+)/', $par_values[$k], $text)) {
                  $hypertext = "<a target=\"_blank\" href=\"" . $text[0] . "\">" . $text[0] . "</a>";
                  $newString = preg_replace('/(http:\/\/[^\s]+)/', $hypertext, $par_values[$k]);
                  $content .= '
                    <li>' . $newString . '</li>';
                }
                elseif (preg_match('/(https:\/\/[^\s]+)/', $par_values[$k], $text)) {
                  $hypertext = "<a target=\"_blank\" href=\"" . $text[0] . "\">" . $text[0] . "</a>";
                  $newString = preg_replace('/(https:\/\/[^\s]+)/', $hypertext, $par_values[$k]);
                  $content .= '
                    <li>' . $newString . '</li>';
                }
                elseif ($par_values[$k]) {
                  $content .= '
                    <li>' . $par_values[$k] . '</li>';
                }
              }
            $content .= '
                  </ul>
                </td>
              </tr>';
          }
        }
      }
      $content .= '
            </table>
          </td>
        </tr>';
    }
  }
  else {
    $content .= t('Nothing');
  }
  $content .= '
      </table>
      <div id="spidercontactnavigation" style="text-align:center;">';
  if ($prod_count > $prod_in_page and $prod_in_page > 0) {
    $r = ceil($prod_count / $prod_in_page);
    $navstyle = 'cursor:pointer;' . (($theme->table_text_size_small != '') ? ('font-size:' . $theme->table_text_size_small . 'px;') : '') . (($theme->text_color != '') ? ('color:#' . $theme->table_text_color . ';') : '');
    $link = url('node/' . $nodeid, array('query' => array('page_num' => ''), 'absolute' => TRUE));
    if ($page_num > 5) {
      $link = url('node/' . $nodeid, array('query' => array('page_num' => '1'), 'absolute' => TRUE));
      $content .= '
        &nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('First') . '</a>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;...&nbsp;';
    }
    if ($page_num > 1) {
      $link = url('node/' . $nodeid, array('query' => array('page_num' => ($page_num - 1)), 'absolute' => TRUE));
      $content .= '
        &nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Prev') . '</a>&nbsp;&nbsp;';
    }
    for ($i = $page_num - 4; $i < ($page_num + 5); $i++) {
      if ($i <= $r && $i >= 1) {
        $link = url('node/' . $nodeid, array('query' => array('page_num' => $i), 'absolute' => TRUE));
        if ($i == $page_num) {
          $content .= '
        <span style="font-weight:bold;color:#000000">&nbsp;' . $i . '&nbsp;</span>';
        }
        else {
          $content .= '
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">&nbsp;' . $i . '&nbsp;</a>';
        }
      }
    }
    if ($page_num < $r) {
      $link = url('node/' . $nodeid, array('query' => array('page_num' => ($page_num + 1)), 'absolute' => TRUE));
      $content .= '
        &nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Next') . '</a>&nbsp;&nbsp;';
    }
    if (($r - $page_num) > 4) {
      $link = url('node/' . $nodeid, array('query' => array('page_num' => $r), 'absolute' => TRUE));
      $content .= '
        &nbsp;...&nbsp;&nbsp;&nbsp;
        <a href=\'javascript:spider_contacts_submit_catal("' . $link . '")\' style="' . $navstyle . '">' . t('Last') . '</a>';
    }
  }
  $content .= '
      </div>
    </div>';
  drupal_add_js('var SpiderCatOFOnLoad = window.onload; window.onload = SpiderCatAddToOnload;', array('type' => 'inline', 'scope' => 'footer'));
  return $content;
}
