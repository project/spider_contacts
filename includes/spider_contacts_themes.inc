<?php
/**
 * @file
 *
 * Contacts themes.
 */

/**
 * Menu loader callback. Load a Spider Contacts themes.
 */
function spider_contacts_themes() {
  $form = array();
  $free_version = '
    <a href="http://web-dorado.com/6-creating-editing-themes/drupal-contacts-guide-step-6-1.html" target="_blank" style="float:right;"><img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/user-manual.png" border="0" alt="' . t('User Manual') . '"></a><div style="clear:both;"></div>
    <div class="messages warning">Themes are disabled in free version. If you need this functionality, you need to buy the commercial version.</div>
    <a href="http://web-dorado.com/products/drupal-contacts-module.html" target="_blank" style="color:red; text-decoration:none; float:right;">
      <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/header.png" border="0" alt="www.web-dorado.com" width="215"><br />
    <div style="float:right;">' . t('Get the full version') . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </a>';
  $form['delete_themes'] = array(
    '#prefix' => $free_version,
    '#type' => 'submit',
    '#submit' => array('spider_contacts_delete_themes'),
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'if (!confirm(Drupal.t("Do you want to delete selected themes?"))) {return false;}'),
  );
  $form['new_theme'] = array(
    '#prefix' => l(t('New'), url('admin/settings/spider_contacts/themes/edit', array('absolute' => TRUE))),
  );
  $header = array(
    'id' => array('data' => t('ID'), 'field' => 'n.id'),
    'title' => array('data' => t('Title'), 'field' => 'n.title'),
    'delete' => array('data' => t('Delete')),
  );
  $options = array();
  $theme_ids = db_select('spider_contacts_themes', 'n')
    ->fields('n', array('id'))
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(20)
    ->execute()
    ->fetchCol();
  foreach ($theme_ids as $theme_id) {
    $row = db_query('SELECT * FROM {spider_contacts_themes} WHERE id=:id', array(':id' => $theme_id))->fetchObject();
    $options[$theme_id] = array(
      'id' => $theme_id,
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $row->title,
          '#href' => url('admin/settings/spider_contacts/themes/edit', array('query' => array('theme_id' => $theme_id), 'absolute' => TRUE)),
        ),
      ),
    );
    if ($row->default_settings != 1 ) {
      $options[$theme_id]['delete'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('Delete'),
          '#href' => url('admin/settings/spider_contacts/themes/delete', array('query' => array('theme_id' => $theme_id), 'absolute' => TRUE)),
        ),
      );
    }
    else {
      $options[$theme_id]['delete'] = '';
    }
  }
  $form['themes_table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No theme available.'),
    '#suffix' => theme('pager', array('tags' => array())),
  );
  foreach ($theme_ids as $theme_id) {
    if ($theme_id <= 1) {
      $form['themes_table'][$theme_id]['#disabled'] = TRUE;
    }
  }
  return $form;
}

/**
 * Delete selected themes.
 */
function spider_contacts_delete_themes($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_themes}")) {
    $theme_ids_col = db_query("SELECT id FROM {spider_contacts_themes}")->fetchCol();
    $flag = FALSE;
    foreach ($theme_ids_col as $theme_id) {
      if (isset($_POST['themes_table'][$theme_id])) {
        $flag = TRUE;
        db_query("DELETE FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id));
        drupal_set_message(t('Selected themes successfully deleted.'), 'status', FALSE);
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one theme.'), 'warning', FALSE);
    }
  }
}

/**
 * Delete theme.
 */
function spider_contacts_theme_delete() {
  if (isset($_GET['theme_id'])) {
    $theme_id = check_plain($_GET['theme_id']);
  }
  db_query("DELETE FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id));
  drupal_set_message(t('Theme successfully deleted.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/themes', array('absolute' => TRUE)));  
}

/**
 * Add or edit theme.
 */
function spider_contacts_theme_edit() {
  drupal_add_css(drupal_get_path('module', 'spider_contacts') . '/css/spider_contacts_theme.css');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_theme_reset.js');
  if (!file_exists("sites/all/libraries/jscolor/jscolor.js")) {
    $message = t('Download "jscolor" library from !jscolor_url and extract it to "sites/all/libraries/jscolor" directory.', array(
      '!jscolor_url' => l(t('here'), 'http://jscolor.com/release/jscolor-1.4.0.zip')));
    drupal_set_message(filter_xss($message), 'warning', FALSE);
  }
  else {
    drupal_add_js('sites/all/libraries/jscolor/jscolor.js');
    $jscolor_class = 'color';
  }
  if (isset($_GET['theme_id'])) {
    $theme_id = check_plain($_GET['theme_id']);
    $title_theme = '';
  }
  else {
    $theme_id = 1;
    $title_theme = 'new ';
    drupal_set_message(t("You can't create a new theme in free version."), 'error', FALSE);
    drupal_goto(url('admin/settings/spider_contacts/themes', array('absolute' => TRUE)));
  }
  $themeinf = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  $title_theme = $title_theme . $themeinf->title;
  $module_background_color = $themeinf->module_background_color;
  $params_background_color1 = $themeinf->params_background_color1;
  $radius = $themeinf->radius;
  $params_background_color2 = $themeinf->params_background_color2;
  $parameters_select_box_width = $themeinf->parameters_select_box_width;
  $border_style = $themeinf->border_style;
  $border_width = $themeinf->border_width;
  $border_color = $themeinf->border_color;
  $text_color = $themeinf->text_color;
  $params_color = $themeinf->params_color;
  $hyperlink_color = $themeinf->hyperlink_color;
  $title_color = $themeinf->title_color;
  $title_background_color = $themeinf->title_background_color;
  $button_background_color = $themeinf->button_background_color;
  $button_color = $themeinf->button_color;
  $count_of_contact_in_the_row = $themeinf->count_of_contact_in_the_row;
  $count_of_rows_in_the_page = $themeinf->count_of_rows_in_the_page;
  $contact_cell_width = $themeinf->contact_cell_width;
  $contact_cell_height = $themeinf->contact_cell_height;
  $small_picture_width = $themeinf->small_picture_width;
  $small_picture_height = $themeinf->small_picture_height;
  $text_size_small = $themeinf->text_size_small;
  $title_size_small = $themeinf->title_size_small;
  $table_background_color = $themeinf->table_background_color;
  $table_params_background_color1 = $themeinf->table_params_background_color1;
  $table_params_background_color2 = $themeinf->table_params_background_color2;
  $table_parameters_select_box_width = $themeinf->table_parameters_select_box_width;
  $table_border_style = $themeinf->table_border_style;
  $table_radius = $themeinf->table_radius;
  $table_border_width = $themeinf->table_border_width;
  $table_border_color = $themeinf->table_border_color;
  $table_text_color = $themeinf->table_text_color;
  $table_params_color1 = $themeinf->table_params_color1;
  $table_params_color2 = $themeinf->table_params_color2;
  $table_title_color = $themeinf->table_title_color;
  $table_title_background_color = $themeinf->table_title_background_color;
  $change_on_hover = $themeinf->change_on_hover;
  $hover_color = $themeinf->hover_color;
  $hover_text_color = $themeinf->hover_text_color;
  $table_button_background_color = $themeinf->table_button_background_color;
  $table_button_color = $themeinf->table_button_color;
  $count_of_rows_in_the_table = $themeinf->count_of_rows_in_the_table;
  $table_text_size_small = $themeinf->table_text_size_small;
  $table_title_size_small = $themeinf->table_title_size_small;
  $table_small_picture_width = $themeinf->table_small_picture_width;
  $table_small_picture_height = $themeinf->table_small_picture_height;
  $cube_background_color = $themeinf->cube_background_color;
  $cube_radius = $themeinf->cube_radius;
  $cube_border_style = $themeinf->cube_border_style;
  $cube_border_width = $themeinf->cube_border_width;
  $cube_border_color = $themeinf->cube_border_color;
  $cube_text_color = $themeinf->cube_text_color;
  $cube_hyperlink_color = $themeinf->cube_hyperlink_color;
  $cube_title_color = $themeinf->cube_title_color;
  $cube_title_background_color = $themeinf->cube_title_background_color;
  $cube_button_background_color = $themeinf->cube_button_background_color;
  $cube_button_color = $themeinf->cube_button_color;
  $cube_count_of_contact_in_the_row = $themeinf->cube_count_of_contact_in_the_row;
  $cube_count_of_rows_in_the_page = $themeinf->cube_count_of_rows_in_the_page;
  $cube_contact_cell_width = $themeinf->cube_contact_cell_width;
  $cube_contact_cell_height = $themeinf->cube_contact_cell_height;
  $cube_small_picture_width = $themeinf->cube_small_picture_width;
  $cube_small_picture_height = $themeinf->cube_small_picture_height;
  $cube_text_size_small = $themeinf->cube_text_size_small;
  $cube_title_size_small = $themeinf->cube_title_size_small;
  $viewcontact_background_color = $themeinf->viewcontact_background_color;
  $viewcontact_border_color = $themeinf->viewcontact_border_color;
  $viewcontact_border_style = $themeinf->viewcontact_border_style;
  $viewcontact_border_width = $themeinf->viewcontact_border_width;
  $viewcontact_params_background_color1 = $themeinf->viewcontact_params_background_color1;
  $viewcontact_params_background_color2 = $themeinf->viewcontact_params_background_color2;
  $viewcontact_params_color = $themeinf->viewcontact_params_color;
  $viewcontact_text_color = $themeinf->viewcontact_text_color;
  $description_text_color = $themeinf->description_text_color;
  $viewcontact_title_background_color = $themeinf->viewcontact_title_background_color;
  $viewcontact_title_color = $themeinf->viewcontact_title_color;
  $full_button_background_color = $themeinf->full_button_background_color;
  $full_button_color = $themeinf->full_button_color;
  $messages_background_color = $themeinf->messages_background_color;
  $large_picture_width = $themeinf->large_picture_width;
  $large_picture_height = $themeinf->large_picture_height;
  $small_pic_size = $themeinf->small_pic_size;
  $text_size_big = $themeinf->text_size_big;
  $title_size_big = $themeinf->title_size_big;
  $viewcontact_radius = $themeinf->viewcontact_radius;
  $default_settings = $themeinf->default_settings;

  $form = array();
  if ($default_settings != '1' || !isset($_GET['theme_id'])) {
    $disable_title_field = FALSE;
    $themes = db_query("SELECT id,title FROM {spider_contacts_themes} WHERE default_settings=:default_settings ORDER BY title", array(':default_settings' => 1))->fetchAllKeyed();
    $themes[0] = '';
    $form['theme_select'] = array(
      '#type' => 'fieldset',
      '#attributes' => array(
        'class' => array('spider_contacts_theme_select_fieldset'),
        'id' => 'spider_contacts_theme_select_fieldset',
      ),
    );
    $form['theme_select']['default_themes'] = array(
      '#type' => 'select',
      '#title' => t('Inherit from theme'),
      '#options' => $themes,
      '#attributes' => array('onchange' => 'spider_contacts_set_theme();'),
      '#default_value' => 0,
    );
  }
  else {
    $disable_title_field = TRUE;
  }
  $free_version = '
    <div class="messages warning">Themes are disabled in free version. If you need this functionality, you need to buy the commercial version.</div>
    <a href="http://web-dorado.com/products/drupal-contacts-module.html" target="_blank" style="color:red; text-decoration:none; float:right;width:100%;">
      <img style="float:right;" src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/header.png" border="0" alt="www.web-dorado.com" width="215"><br /><br /><br />
      <div style="float:right;">' . t('Get the full version') . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </a>';
  $form['theme_global'] = array(
    '#type' => 'fieldset',
    '#prefix' => $free_version,
    '#title' => t('Full View Options'),
    '#attributes' => array(
      'class' => array('spider_contacts_theme_global_fielset'),
      'id' => 'theme_global',
    ),
  );
  $form['theme_global']['theme_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $title_theme,
    '#disabled' => $disable_title_field,
    '#size' => 25,
  );
  $form['theme_global']['module_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#default_value' => $module_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['params_background_color1'] = array(
    '#type' => 'textfield',
    '#title' => t('The First Background Color of Parameters'),
    '#default_value' => $params_background_color1,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['radius'] = array(
    '#type' => 'radios',
    '#title' => t('Rounded Corners'),
    '#default_value' => $radius,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['theme_global']['params_background_color2'] = array(
    '#type' => 'textfield',
    '#title' => t('The Second Background Color of Parameters'),
    '#default_value' => $params_background_color2,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['parameters_select_box_width'] = array(
    '#type' => 'textfield',
    '#title' => t('The Width of Parameters Box'),
    '#default_value' => $parameters_select_box_width,
    '#size' => 25,
  );
  $border_style_select = array(
    'solid' => t('Solid'),
    'double' => t('Double'),
    'dashed' => t('Dashed'),
    'dotted' => t('Dotted'),
    'groove' => t('Groove'),
    'inset' => t('Inset'),
    'outset' => t('Outset'),
    'ridge' => t('Ridge'),
  );
  $form['theme_global']['border_style'] = array(
    '#type' => 'select',
    '#title' => t('Border Style'),
    '#default_value' => $border_style,
    '#options' => $border_style_select,
  );
  $form['theme_global']['border_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Border Width'),
    '#default_value' => $border_width,
    '#size' => 25,
  );
  $form['theme_global']['border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Border Color'),
    '#default_value' => $border_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Color'),
    '#default_value' => $text_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['params_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameter Values Color'),
    '#default_value' => $params_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['hyperlink_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Hyperlink Color'),
    '#default_value' => $hyperlink_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['title_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Color'),
    '#default_value' => $title_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['title_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Background Color'),
    '#default_value' => $title_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['button_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Background color'),
    '#default_value' => $button_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['button_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text Color'),
    '#default_value' => $button_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_global']['count_of_contact_in_the_row'] = array(
    '#type' => 'textfield',
    '#title' => t('Count of Contacts in the Row'),
    '#default_value' => $count_of_contact_in_the_row,
    '#size' => 25,
  );
  $form['theme_global']['count_of_rows_in_the_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Count of Rows in the Page'),
    '#default_value' => $count_of_rows_in_the_page,
    '#size' => 25,
  );
  $form['theme_global']['contact_cell_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Cell Width'),
    '#description' => t('In pixels.'),
    '#default_value' => $contact_cell_width,
    '#size' => 25,
  );
  $form['theme_global']['contact_cell_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Cell Height'),
    '#description' => t('In pixels.'),
    '#default_value' => $contact_cell_height,
    '#size' => 25,
  );
  $form['theme_global']['small_picture_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Width'),
    '#description' => t('In pixels.'),
    '#default_value' => $small_picture_width,
    '#size' => 25,
  );
  $form['theme_global']['small_picture_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Height'),
    '#description' => t('In pixels.'),
    '#default_value' => $small_picture_height,
    '#size' => 25,
  );
  $form['theme_global']['text_size_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Size'),
    '#default_value' => $text_size_small,
    '#size' => 25,
  );
  $form['theme_global']['title_size_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Size'),
    '#default_value' => $title_size_small,
    '#size' => 25,
  );
  if (isset($_GET['theme_id']) && $default_settings == 1) {
    $form['theme_global']['reset_button'] = array(
      '#prefix' => '<div id="reset_theme_id" onclick="spider_contacts_reset_theme_' . $theme_id . '();" style="cursor:pointer; width:170px; height:35px; color:#ffffff; font-size:15px; background-repeat:no-repeat; background-image:url(\'' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/reset_theme.png\')">
                      <p style="font-weight: bold; padding-left: 51px; padding-top: 7px; margin:0px;">' . t('Reset Theme') . '</p>
                    </div>',
    );
  }
  $form['theme_cells_page_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Table View Options'),
    '#attributes' => array(
      'class' => array('spider_contacts_theme_cells_page_options_fielset'),
      'id' => 'theme_cells_page_options',
    ),
  );
  $form['theme_cells_page_options']['table_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Table Background Color'),
    '#default_value' => $table_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_params_background_color1'] = array(
    '#type' => 'textfield',
    '#title' => t('The First Background Color of Parameters'),
    '#default_value' => $table_params_background_color1,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_params_background_color2'] = array(
    '#type' => 'textfield',
    '#title' => t('The Second Background Color of Parameters'),
    '#default_value' => $table_params_background_color2,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_parameters_select_box_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameters Box Width'),
    '#description' => t('In pixels'),
    '#default_value' => $table_parameters_select_box_width,
    '#size' => 25,
  );
  $form['theme_cells_page_options']['table_border_style'] = array(
    '#type' => 'select',
    '#title' => t('Border Style'),
    '#default_value' => $table_border_style,
    '#options' => $border_style_select,
  );
  $form['theme_cells_page_options']['table_radius'] = array(
    '#type' => 'radios',
    '#title' => t('Rounded Corners'),
    '#default_value' => $table_radius,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['theme_cells_page_options']['table_border_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Border Width'),
    '#description' => t('In pixels'),
    '#default_value' => $table_border_width,
    '#size' => 25,
  );
  $form['theme_cells_page_options']['table_border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Border Color'),
    '#default_value' => $table_border_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Color'),
    '#default_value' => $table_text_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_params_color1'] = array(
    '#type' => 'textfield',
    '#title' => t('The First Text Color of Parameter Values'),
    '#default_value' => $table_params_color1,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_params_color2'] = array(
    '#type' => 'textfield',
    '#title' => t('The Second Text Color of Parameter Values'),
    '#default_value' => $table_params_color2,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_title_color'] = array(
    '#type' => 'textfield',
    '#title' => t('The Text Color of the Title Row'),
    '#default_value' => $table_title_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_title_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('The Background Color of the Title Row'),
    '#default_value' => $table_title_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['change_on_hover'] = array(
    '#type' => 'radios',
    '#title' => t('Change background of table row on Hover'),
    '#default_value' => $change_on_hover,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['theme_cells_page_options']['hover_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color on Hover'),
    '#default_value' => $hover_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['hover_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Color on Hover'),
    '#default_value' => $hover_text_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_button_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('The Background Color of the Button'),
    '#default_value' => $table_button_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['table_button_color'] = array(
    '#type' => 'textfield',
    '#title' => t('The Text Color of the Button'),
    '#default_value' => $table_button_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_cells_page_options']['count_of_rows_in_the_table'] = array(
    '#type' => 'textfield',
    '#title' => t('Count of Rows in the Table'),
    '#default_value' => $count_of_rows_in_the_table,
    '#size' => 25,
  );
  $form['theme_cells_page_options']['table_text_size_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Size'),
    '#description' => t('In pixels.'),
    '#default_value' => $table_text_size_small,
    '#size' => 25,
  );
  $form['theme_cells_page_options']['table_title_size_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Text Size'),
    '#description' => t('In pixels.'),
    '#default_value' => $table_title_size_small,
    '#size' => 25,
  );
  $form['theme_cells_page_options']['table_small_picture_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Width'),
    '#description' => t('In pixels.'),
    '#default_value' => $table_small_picture_width,
    '#size' => 25,
  );
  $form['theme_cells_page_options']['table_small_picture_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Height'),
    '#description' => t('In pixels.'),
    '#default_value' => $table_small_picture_height,
    '#size' => 25,
  );
  $form['theme_list_page_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Short View Options'),
    '#attributes' => array(
      'class' => array('spider_contacts_theme_list_page_options_fielset'),
      'id' => 'theme_list_page_options',
    ),
  );
  $form['theme_list_page_options']['cube_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color'),
    '#default_value' => $cube_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_radius'] = array(
    '#type' => 'radios',
    '#title' => t('Rounded Corners'),
    '#default_value' => $cube_radius,
    '#options' => array('1' => t('Enable'), '0' => t('Disable')),
  );
  $form['theme_list_page_options']['cube_border_style'] = array(
    '#type' => 'select',
    '#title' => t('Border Style'),
    '#default_value' => $cube_border_style,
    '#options' => $border_style_select,
  );
  $form['theme_list_page_options']['cube_border_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Border Width'),
    '#description' => t('In pixels.'),
    '#default_value' => $cube_border_width,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Border Color'),
    '#default_value' => $cube_border_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Color'),
    '#default_value' => $cube_text_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_hyperlink_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Hyperlink Color'),
    '#default_value' => $cube_hyperlink_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_title_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Text Color'),
    '#default_value' => $cube_title_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_title_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Background Color'),
    '#default_value' => $cube_title_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_button_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Background Color'),
    '#default_value' => $cube_button_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_button_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text Color'),
    '#default_value' => $cube_button_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_list_page_options']['cube_count_of_contact_in_the_row'] = array(
    '#type' => 'textfield',
    '#title' => t('Count of Contacts in the Row'),
    '#default_value' => $cube_count_of_contact_in_the_row,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_count_of_rows_in_the_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Count of Rows in the Page'),
    '#default_value' => $cube_count_of_rows_in_the_page,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_contact_cell_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Cell Width'),
    '#description' => t('In pixels.'),
    '#default_value' => $cube_contact_cell_width,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_contact_cell_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Cell Height'),
    '#description' => t('In pixels.'),
    '#default_value' => $cube_contact_cell_height,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_small_picture_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Width'),
    '#description' => t('In pixels.'),
    '#default_value' => $cube_small_picture_width,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_small_picture_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Height'),
    '#description' => t('In pixels.'),
    '#default_value' => $cube_small_picture_height,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_text_size_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Size'),
    '#default_value' => $cube_text_size_small,
    '#size' => 25,
  );
  $form['theme_list_page_options']['cube_title_size_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Text Size'),
    '#default_value' => $cube_title_size_small,
    '#size' => 25,
  );
  $form['theme_product_page_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Page Options'),
    '#attributes' => array(
      'class' => array('spider_contacts_theme_product_page_options_fielset'),
      'id' => 'theme_product_page_options',
    ),
  );
  $form['theme_product_page_options']['viewcontact_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Background Color'),
    '#default_value' => $viewcontact_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['viewcontact_border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Border Color'),
    '#default_value' => $viewcontact_border_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['viewcontact_border_style'] = array(
    '#type' => 'select',
    '#title' => t('Border Style'),
    '#default_value' => $viewcontact_border_style,
    '#options' => $border_style_select,
  );
  $form['theme_product_page_options']['viewcontact_border_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameters Border Width '),
    '#description' => t('In pixels.'),
    '#default_value' => $viewcontact_border_width,
    '#size' => 25,
  );
  $form['theme_product_page_options']['viewcontact_params_background_color1'] = array(
    '#type' => 'textfield',
    '#title' => t('The First Background Color of Parameters'),
    '#default_value' => $viewcontact_params_background_color1,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['viewcontact_params_background_color2'] = array(
    '#type' => 'textfield',
    '#title' => t('The Second Background Color of Parameters'),
    '#default_value' => $viewcontact_params_background_color2,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['viewcontact_params_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameters Value Text Color'),
    '#default_value' => $viewcontact_params_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['viewcontact_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Color'),
    '#default_value' => $viewcontact_text_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['description_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Description Text Color'),
    '#default_value' => $description_text_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['viewcontact_title_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Background Color'),
    '#default_value' => $viewcontact_title_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['viewcontact_title_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Text Color'),
    '#default_value' => $viewcontact_title_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['full_button_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Background Color'),
    '#default_value' => $full_button_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['full_button_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text Color'),
    '#default_value' => $full_button_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['messages_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Message Box Background Color'),
    '#default_value' => $messages_background_color,
    '#size' => 25,
    '#attributes' => array('class' => array('color')),
  );
  $form['theme_product_page_options']['large_picture_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Width'),
    '#description' => t('In pixels.'),
    '#default_value' => $large_picture_width,
    '#size' => 25,
  );
  $form['theme_product_page_options']['large_picture_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Picture Height'),
    '#description' => t('In pixels.'),
    '#default_value' => $large_picture_height,
    '#size' => 25,
  );
  $form['theme_product_page_options']['small_pic_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Small Pictures Height'),
    '#description' => t('In pixels.'),
    '#default_value' => $small_pic_size,
    '#size' => 25,
  );
  $form['theme_product_page_options']['text_size_big'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Size'),
    '#default_value' => $text_size_big,
    '#size' => 25,
  );
  $form['theme_product_page_options']['title_size_big'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Name Text Size'),
    '#default_value' => $title_size_big,
    '#size' => 25,
  );
  $form['theme_product_page_options']['viewcontact_radius'] = array(
    '#type' => 'radios',
    '#title' => t('Rounded Corners'),
    '#default_value' => $viewcontact_radius,
    '#size' => 25,
    '#options' => array(1 => t('Enable'), 0 => t('Disable')),
  );
  $form['theme_save'] = array(
    '#type' => 'submit',
    '#prefix' => '<div style="float:left; bottom:0px; clear:left;">',
    '#value' => t('Save'),
    '#attributes' => array('onclick' => 'alert(Drupal.t("You cant save the changes in free version."));return false;'),
  );
  $form['theme_apply'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#attributes' => array('onclick' => 'alert(Drupal.t("You cant save the changes in free version."));return false;'),
  );
  $form['theme_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('spider_contacts_theme_cancel'),
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Cancel theme save.
 */
function spider_contacts_theme_cancel($form, &$form_state) {
  $form_state['redirect'] = url('admin/settings/spider_contacts/themes', array('absolute' => TRUE));
}
