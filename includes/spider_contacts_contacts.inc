<?php
/**
 * @file
 * Spider Contacts module contacts.
 */

/**
 * Menu loader callback. Load contacts table.
 */
function spider_contacts_contacts() {
  $form = array();
  $free_version = '<a href="http://web-dorado.com/drupal-contacts-guide-step-3.html" target="_blank" style="float:right;"><img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/user-manual.png" border="0" alt="' . t('User Manual') . '"></a><div style="clear:both;"></div>
  <a href="http://web-dorado.com/products/drupal-contacts-module.html" target="_blank" style="color:red; text-decoration:none; float:right;">
                    <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/header.png" border="0" alt="www.web-dorado.com" width="215"><br />
                  <div style="float:right;">' . t('Get the full version') . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
                  </a>';
  $form['fieldset_contact_buttons'] = array(
    '#prefix' => $free_version,
    '#type' => 'fieldset',
  );
  $form['fieldset_contact_buttons']['publish_contact'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_contacts_publish'),
    '#value' => t('Publish'),
  );
  $form['fieldset_contact_buttons']['unpublish_contact'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_contacts_unpublish'),
    '#value' => t('Unpublish'),
  );
  $form['fieldset_contact_buttons']['delete_contact'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_contacts_delete'),
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'if (!confirm(Drupal.t("Do you want to delete selected contacts?"))) {return false;}'),
  );
  $form['fieldset_contact_buttons']['save_order'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_contact_save_ordering'),
    '#value' => t('Save order'),
  );
  $form['fieldset_contact_buttons']['new_contact'] = array(
    '#prefix' => l(t('New'), url('admin/settings/spider_contacts/contacts/edit', array('absolute' => TRUE))),
    '#suffix' => '<input type="button" value="' . t('Export as CSV') . '" onclick=\'window.location="' . url('spider_contacts/generate_csv', array('absolute' => TRUE)) . '"\' class="form-submit" style="float:right;">',
  );
  $form['fieldset_search_contacts_name'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search contacts by name'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_contacts_search_contacts_name', '') == '') ? TRUE : FALSE),
  );
  $form['fieldset_search_contacts_name']['search_contacts_name'] = array(
    '#type' => 'textfield',
    '#size' => 25,
    '#default_value' => variable_get('spider_contacts_search_contacts_name', ''),
  );
  $form['fieldset_search_contacts_name']['search_contacts'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_search_contacts_name'),
    '#value' => t('Go'),
  );
  $form['fieldset_search_contacts_name']['reset_contacts'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('spider_contacts_reset_contacts_name'),
  );
  // Search contacts by category.
  $form['fieldset_search_contacts_by_category'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search contacts by category'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_contacts_search_contacts_by_category', '') == '') ? TRUE : FALSE),
  );
  $form['fieldset_search_contacts_by_category']['search_contacts_by_category_select'] = array(
    '#type' => 'select',
    '#options' => db_query("SELECT id,name FROM {spider_contacts_contacts_categories} ORDER BY name")->fetchAllKeyed(),
    '#empty_option' => t('-Select Category-'),
    '#default_value' => variable_get('spider_contacts_search_contacts_by_category', ''),
  );
  $form['fieldset_search_contacts_by_category']['search_contacts_by_category'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_search_contacts_by_category'),
    '#value' => t('Go '),
  );
  $form['fieldset_search_contacts_by_category']['reset_contacts_by_category'] = array(
    '#type' => 'submit',
    '#value' => t('Reset '),
    '#submit' => array('spider_contacts_reset_contacts_by_category'),
  );
  drupal_add_tabledrag('contacts_table-components', 'order', 'sibling', 'weight');
  $header = array(
    'checkbox' => array('class' => 'select-all'),
    'id' => array('data' => t('ID'), 'field' => 'n.id'),
    'first_name' => array('data' => t('First Name'), 'field' => 'n.first_name'),
    'last_name' => array('data' => t('Last Name'), 'field' => 'n.last_name'),
    'order' => array('data' => t('Order'), 'field' => 'n.ordering', 'sort' => 'asc'),
    'category' => array('data' => t('Category'), 'field' => 'n.category_id'),
    'published' => array('data' => t('Published')),
    'delete' => array('data' => t('Delete')),
  );
  $options = array();
  $query = db_select('spider_contacts_contacts', 'n')
    ->fields('n', array('id'))
    ->condition(
      db_or()
        ->condition('n.first_name', '%' . db_like(variable_get('spider_contacts_search_contacts_name', '')) . '%', 'LIKE')
        ->condition('n.last_name', '%' . db_like(variable_get('spider_contacts_search_contacts_name', '')) . '%', 'LIKE')
    )
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(20);
  if (variable_get('spider_contacts_search_contacts_by_category', '') != '') {
    $query = $query->condition('n.category_id', variable_get('spider_contacts_search_contacts_by_category', ''), '=');
  }
  $contacts_ids = $query->execute()->fetchCol();
  foreach ($contacts_ids as $contact_id) {
    $row = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $contact_id))->fetchObject();
    if ($row->published) {
      $publish_unpublish_png = 'publish.png';
      $publish_unpublish_function = 'unpublish';
    }
    else {
      $publish_unpublish_png = 'unpublish.png';
      $publish_unpublish_function = 'publish';
    }
    $category_name = db_query("SELECT name FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $row->category_id))->fetchField();
    if ($row->category_id == 0) {
      $category_name = t('Uncategorised');
    } 
    $options[$contact_id] = array(
      'checkbox' => array(
        'data' => array(
          '#type' => 'checkbox',
          '#attributes' => array('name' => 'spider_contacts_contact_check_' . $contact_id),
        ),
      ),
      'id' => $contact_id,
      'first_name' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $row->first_name,
          '#href' => url('admin/settings/spider_contacts/contacts/edit', array('query' => array('contact_id' => $contact_id), 'absolute' => TRUE)),
        ),
      ),
      'last_name' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $row->last_name,
          '#href' => url('admin/settings/spider_contacts/contacts/edit', array('query' => array('contact_id' => $contact_id), 'absolute' => TRUE)),
        ),
      ),
      'order' => array(
        'data' => array(
          '#type' => 'textfield',
          '#size' => 3,
          '#value' => $row->ordering,
          '#attributes' => array('name' => 'spider_contacts_contacts_order_' . $contact_id, 'class' => array('weight')),
        ),
      ),
      'category' => $category_name,
      'published' => l(t('<img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/' . $publish_unpublish_png . '" />'), url('admin/settings/spider_contacts/contacts/' . $publish_unpublish_function, array('query' => array('contact_id' => $contact_id), 'absolute' => TRUE)), array('html' => TRUE)),
      'delete' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('Delete'),
          '#href' => url('admin/settings/spider_contacts/contacts/delete', array('query' => array('contact_id' => $contact_id), 'absolute' => TRUE)),
        ),
      ),
    );
    $options[$contact_id]['#attributes'] = array('class' => array('draggable'));
  }
  $form['contacts_table'] = array(
    '#type' => 'tableselect',
    '#js_select' => TRUE,
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No contacts available.'),
    '#suffix' => theme('pager', array('tags' => array())),
    '#attributes' => array('id' => 'contacts_table-components'),
  );
  
  foreach ($contacts_ids as $contact_id) {
    $form['contacts_table'][$contact_id]['#disabled'] = TRUE;
  }
  return $form;
}

/**
 * Search contacts by name.
 */
function spider_contacts_search_contacts_name($form, &$form_state) {
  if ($form_state['values']['search_contacts_name'] != '') {
    variable_set('spider_contacts_search_contacts_name', $form_state['values']['search_contacts_name']);
  }
  else {
    variable_set('spider_contacts_search_contacts_name', '');
  }
  $form_state['redirect'] = url('admin/settings/spider_contacts/contacts', array('absolute' => TRUE));
}

/**
 * Reset contacts by name.
 */
function spider_contacts_reset_contacts_name($form, &$form_state) {
  variable_set('spider_contacts_search_contacts_name', '');
  $form_state['redirect'] = url('admin/settings/spider_contacts/contacts', array('absolute' => TRUE));
}

/**
 * Search contacts by category.
 */
function spider_contacts_search_contacts_by_category($form, &$form_state) {
  if ($form_state['values']['search_contacts_by_category_select'] != '') {
    variable_set('spider_contacts_search_contacts_by_category', $form_state['values']['search_contacts_by_category_select']);
  }
  else {
    variable_set('spider_contacts_search_contacts_by_category', '');
  }
}

/**
 * Reset contacts by category.
 */
function spider_contacts_reset_contacts_by_category($form, &$form_state) {
  variable_set('spider_contacts_search_contacts_by_category', '');
}

/**
 * Publish selected contacts.
 */
function spider_contacts_contacts_publish($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_contacts}")) {
    $contact_ids_col = db_query("SELECT id FROM {spider_contacts_contacts}")->fetchCol();
    $flag = FALSE;
    foreach ($contact_ids_col as $contact_id) {
      if (isset($_POST['spider_contacts_contact_check_' . $contact_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_contacts_contacts} SET published=:published WHERE id=:id", array(':published' => 1, ':id' => $contact_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one contact.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Contacts successfully published.'), 'status', FALSE);
    }
  }
}

/**
 * Publish contact.
 */
function spider_contacts_contact_publish() {
  if (isset($_GET['contact_id'])) {
    $contact_id = check_plain($_GET['contact_id']);
    db_query("UPDATE {spider_contacts_contacts} SET published=:published WHERE id=:id", array(':published' => 1, ':id' => $contact_id));
  }
  drupal_set_message(t('Contact successfully published.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/contacts', array('absolute' => TRUE)));
}

/**
 * Unpublish selected contacts.
 */
function spider_contacts_contacts_unpublish($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_contacts}")) {
    $contact_ids_col = db_query("SELECT id FROM {spider_contacts_contacts}")->fetchCol();
    $flag = FALSE;
    foreach ($contact_ids_col as $contact_id) {
      if (isset($_POST['spider_contacts_contact_check_' . $contact_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_contacts_contacts} SET published=:published WHERE id=:id", array(':published' => 0, ':id' => $contact_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one contact.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Contacts successfully unpublished.'), 'status', FALSE);
    }
  }
}

/**
 * Unpublish contact.
 */
function spider_contacts_contact_unpublish() {
  if (isset($_GET['contact_id'])) {
    $contact_id = check_plain($_GET['contact_id']);
    db_query("UPDATE {spider_contacts_contacts} SET published=:published WHERE id=:id", array(':published' => 0, ':id' => $contact_id));
  }
  drupal_set_message(t('Contact successfully unpublished.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/contacts', array('absolute' => TRUE)));
}

/**
 * Delete selected contacts.
 */
function spider_contacts_contacts_delete($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_contacts}")) {
    $contact_ids_col = db_query("SELECT id FROM {spider_contacts_contacts}")->fetchCol();
    $flag = FALSE;
    foreach ($contact_ids_col as $contact_id) {
      if (isset($_POST['spider_contacts_contact_check_' . $contact_id])) {
        $flag = TRUE;
        db_query("DELETE FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $contact_id));
        drupal_set_message(t('Selected contacts successfully deleted.'), 'status', FALSE);
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one contact.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Contacts successfully deleted.'), 'status', FALSE);
    }
  }
}

/**
 * Delete contact.
 */
function spider_contacts_contact_delete() {
  if (isset($_GET['contact_id'])) {
    $contact_id = check_plain($_GET['contact_id']);
    db_query("DELETE FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $contact_id));
  }
  drupal_set_message(t('Contact successfully deleted.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/contacts', array('absolute' => TRUE)));
}

/**
 * Save ordering.
 */
function spider_contacts_contact_save_ordering($form, &$form_state) {
  $contacts_ids = db_query("SELECT id FROM {spider_contacts_contacts}")->fetchCol();
  foreach ($contacts_ids as $contact_id) {
    if (isset($_POST['spider_contacts_contacts_order_' . $contact_id])) {
      $ordering = check_plain($_POST['spider_contacts_contacts_order_' . $contact_id]);
      db_query("UPDATE {spider_contacts_contacts} SET ordering=:ordering WHERE id=:id", array(':ordering' => $ordering, ':id' => $contact_id));
    }
    drupal_set_message(t('Contacts ordering successfully saved.'), 'status', FALSE);
  }
}

/**
 * Add or edit contact.
 */
function spider_contacts_contact_edit() {
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_attach_file.js');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_params.js');
  if (isset($_GET['contact_id']) && ($_GET['contact_id'] != '')) {
    $contact_id = check_plain($_GET['contact_id']);
    $row = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $contact_id))->fetchObject();
    $contact_first_name = $row->first_name;
    $contact_last_name = $row->last_name;
    $contact_email = $row->email;
    $contact_want_email = $row->want_email;
    $contact_images = $row->image_url;
    $images_array = explode('#***#', $contact_images);
    $contact_description = $row->description;
    $contact_short_description = $row->short_description;
    $contact_published = $row->published;
    if (isset($_GET['category_id'])) {
      $contact_category_id = check_plain($_GET['category_id']);
      $category_param = db_query("SELECT param FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $contact_category_id))->fetchField();
      $contact_param = str_replace('#***#', '#@@@#', $category_param);
    }
    else {
      $contact_category_id = $row->category_id;
      $contact_param = $row->param;
    }
  }
  else {
    if ((isset($_GET['category_id'])) && ($_GET['category_id'] != 0)) {
      $contact_category_id = check_plain($_GET['category_id']);
      $category_param = db_query("SELECT param FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $contact_category_id))->fetchField();
      $contact_param = str_replace('#***#', '#@@@#', $category_param);
      $contact_first_name = '';
      $contact_last_name = '';
      $contact_email = '';
      $contact_want_email = 0;
      $contact_description = '';
      $contact_short_description = '';
    }
    else {
      $contact_category_id = 0;
      $contact_param = '';
      $contact_first_name = '';
      $contact_last_name = '';
      $contact_email = '';
      $contact_want_email = 0;
      $contact_description = '';
      $contact_short_description = '';
    }
    $contact_id = '';
    $contact_images = '';
    $images_array = '';
    $contact_published = 1;
  }
  drupal_add_js(array(
    'spider_contacts' => array(
      'delete_png_url' => base_path() . drupal_get_path('module', 'spider_contacts') . '/images/',
      'select_action' => url('admin/settings/spider_contacts/contacts/edit', array('query' => array('contact_id' => $contact_id), 'absolute' => TRUE)),
    ),
    ),
    'setting');
  $categories_id_array[0] = t('Uncategorised');
  $categories_id_array += db_query("SELECT id,name FROM {spider_contacts_contacts_categories} ORDER BY name")->fetchAllKeyed();
  if (file_exists("sites/all/libraries/tinymce/jscripts/tiny_mce/tiny_mce.js")) {
    drupal_add_js('sites/all/libraries/tinymce/jscripts/tiny_mce/tiny_mce.js');
    drupal_add_js('tinyMCE.init({
				// General options
        mode : "specific_textareas",
        editor_selector : "spider_contacts_editor",
				theme : "advanced",
				plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
				// Theme options
				theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,

				// Skin options
				skin : "o2k7",
				skin_variant : "silver",

				// Example content CSS (should be your site CSS)
				//content_css : "css/example.css",
				
				// Drop lists for link/image/media/template dialogs
				template_external_list_url : "js/template_list.js",
				external_link_list_url : "js/link_list.js",
				external_image_list_url : "js/image_list.js",
				media_external_list_url : "js/media_list.js",

				// Replace values for the template plugin
				template_replace_values : {
					username : "Some User",
					staffid : "991234"
				}
			});', array('type' => 'inline', 'scope' => 'footer'));
  }
  $form = array();
  drupal_add_js('
    var contact_images_array = "' . $contact_images . '";
    var parameters0 = new Array();
    parameters0["sel2"] = contact_images_array.split("#***#");', array('type' => 'inline'));
  $form['contact_first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#required' => TRUE,
    '#default_value' => $contact_first_name,
    '#size' => 40,
  );
  $form['contact_last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#default_value' => $contact_last_name,
    '#size' => 40,
  );
  $form['contact_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => $contact_email,
    '#size' => 40,
    '#attributes' => array('onchange' => 'spider_contacts_email_validation("edit-contact-email");'),
  );
  $form['contact_want_email'] = array(
    '#type' => 'radios',
    '#title' => t('Send Email when Message Sent'),
    '#default_value' => $contact_want_email,
    '#options' => array(1 => t('Yes'), 0 => t('No')),
  );
  $form['contact_categories'] = array(
    '#type' => 'select',
    '#title' => t('Category'),
    '#default_value' => $contact_category_id,
    '#options' => $categories_id_array,
    '#attributes' => array('onchange' => 'spider_contacts_change_category();'),
  );
  $par_image = explode("#***#", $contact_images);
  $k = 0;
  $image_items = '<strong>' . t('Images') . '</strong><div id="sel2">';
  $show_images = '<div id="images_div" style="width:470px;">';
  while ($k < 1000) {
    if (isset($par_image[$k]) && $par_image[$k] != '') {
      $image_items .= '
        <input type="text" class="form-text" style="width:200px;" id="inp_sel2_' . $k . '" value="' . $par_image[$k] . '" onChange=\'spider_contacts_add("sel2");\' />
        <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/delete.png" style="cursor:pointer;" onclick=\'spider_contacts_remove(' . $k . ', "sel2");spider_contacts_delete_view_image(' . $k . ');\'><br />';
      $show_images .= '
        <a id="view_image_' . $k . '" title="' . $par_image[$k] . '" target="_blank" href="' . $par_image[$k] . '">
          <img style="max-height:50px; max-width:50px; margin:8px 0 8px 8px;" src="' . $par_image[$k] . '" />
        </a>
        <img style="cursor:pointer; margin:8px 0 0 0; vertical-align:top;" id="view_image_delete_' . $k . '" src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/delete.png" onclick=\'spider_contacts_remove(' . $k . ', "sel2");spider_contacts_delete_view_image(' . $k . ');\'>';
      $k++;
    }
    else {
      $image_items .= '
        <input type="text" class="form-text" style="width:200px;" id="inp_sel2_' . $k . '" value="" onChange=\'spider_contacts_add("sel2")\' />
        <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/delete.png" style="cursor:pointer;" onclick=\'spider_contacts_remove(' . $k . ', "sel2");\'><br />';
      $k = 1000;
    }
  }
  $show_images .= '</div>';
  $image_items .= '</div>
                   <input type="hidden" name="images" id="hid_sel2" value="' . $contact_images . '" />
                   <div class="description">' . t('Enter image url.') . '</div>';
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['contact_add_image_button'] = array(
    '#prefix' => $image_items . '<div id="input_file_container"></div>',
    '#type' => 'button',
    '#value' => t('Add Image'),
    '#attributes' => array('onclick' => 'spider_contacts_add_upload_box(); return false;'),
    '#suffix' => $show_images,
  );
  $message_for_without_editor = '
    <div class="messages error" style="width:590px;">
      ' . t('To show HTML editor download "tinymce" library from !url and extract it into "sites/all/libraries/tinymce" directory.', array('!url' => l(t('here'), 'http://github.com/downloads/tinymce/tinymce/tinymce_3.5.7.zip'))) . '
    </div>';
  $form['contact_short_description'] = array(
    '#prefix' => (file_exists("sites/all/libraries/tinymce/jscripts/tiny_mce/tiny_mce.js") ? '' : $message_for_without_editor) . '<div style="width:650px;">',
    '#type' => 'textarea',
    '#title' => t('Short Description'),
    '#default_value' => $contact_short_description,
    '#attributes' => array('class' => array('spider_contacts_editor')),
    '#description' => t('Short description will be shown in the contact list.'),
    '#resizable' => FALSE,
  );
  $form['contact_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $contact_description,
    '#attributes' => array('class' => array('spider_contacts_editor')),
    '#resizable' => FALSE,
    '#suffix' => '</div>',
  );
  if (!isset($_POST['category_id'])) {
    $script_tag = 'script';
  }
  else {
    $script_tag = 'webdorado';
  }
  $param_items = '';
  $params_array = explode('#@@@#', $contact_param);
  foreach ($params_array as $key => $params) {
    $param = explode("@@:@@", $params);
    if ($param[0] != '') {
      if (!isset($param[1])) {
        $param[1] = '';
      }
      $param_items .= '<strong>' . $param[0] . '</strong><div id="sel1_' . $param[0] . '">';
      $par = explode("#***#", $param[1]);
      $param_items .= '<' . $script_tag . '>
        var contact_parameters_array = "' . $param[1] . '";
        parameters0["sel1_' . $param[0] . '"] = contact_parameters_array.split("#***#");</' . $script_tag . '>';
      $k = 0;
      while ($k < 1000) {
        if (isset($par[$k]) && $par[$k] != '') {
          $param_items .= '
            <input type="text" class="form-text" style="width:200px;" id="inp_sel1_' . $param[0] . '_' . $k . '" value="' . $par[$k] . '" onChange=\'spider_contacts_add("sel1_' . $param[0] . '");\' />
            <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/delete.png" style="cursor:pointer;" onclick=\'spider_contacts_remove(' . $k . ', "sel1_' . $param[0] . '");\'><br />';
          $k++;
        }
        else {
          $param_items .= '
            <input type="text" class="form-text" style="width:200px;" id="inp_sel1_' . $param[0] . '_' . $k . '" value="" onChange=\'spider_contacts_add("sel1_' . $param[0] . '")\' />
            <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/delete.png" style="cursor:pointer;" onclick=\'spider_contacts_remove(' . $k . ', "sel1_' . $param[0] . '");\'><br />';
          $k = 1000;
        }
      }
      $param_items .= '</div>
                       <input type="hidden" name="param_' . $key . '" id="hid_sel1_' . $param[0] . '" value="' . $param[1] . '" />
                       <div class="description">' . t('Enter') . ' ' . $param[0] . '</div>';
    }
  }
  $form['contact_published'] = array(
    '#prefix' => '<div id="category_parameters">' . $param_items . '</div>',
    '#type' => 'radios',
    '#title' => t('Published'),
    '#default_value' => $contact_published,
    '#options' => array('1' => t('Yes'), '0' => t('No')),
  );
  $form['contact_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('spider_contacts_contact_save'),
    '#attributes' => array('onclick' => 'if (!spider_contacts_email_validation("edit-contact-email")) {return false;}'),
  );
  $form['contact_apply'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#submit' => array('spider_contacts_contact_apply'),
    '#attributes' => array('onclick' => 'if (!spider_contacts_email_validation("edit-contact-email")) {return false;}'),
  );
  $form['contact_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#attributes' => array('onclick' => 'document.getElementById("edit-contact-first-name").setAttribute("style", "color:rgba(255, 0, 0, 0)");document.getElementById("edit-contact-first-name").setAttribute("value", "cancel");'),
    '#submit' => array('spider_contacts_contact_cancel'),
  );
  // Check if post max size is smaller than this form post.
  $post_max_size_error = error_get_last();
  if ($post_max_size_error) {
    drupal_set_message($post_max_size_error['message'] . t('. Change "post_max_size" in .htaccess file.'), 'error', FALSE);
  }
  return $form;
}

/**
 * Save contact.
 */
function spider_contacts_contact_save($form, &$form_state) {
  $contact_first_name = $form_state['values']['contact_first_name'];
  $contact_last_name = $form_state['values']['contact_last_name'];
  $contact_email = $form_state['values']['contact_email'];
  $contact_want_email = $form_state['values']['contact_want_email'];
  $contact_category_id = $form_state['values']['contact_categories'];
  $dir = 'public://';
  $upload_event = array();
  if ($_FILES) {
    foreach ($_FILES['files']['name'] as $key => $value) {
      if (drupal_substr($key, 0, 12) == 'image_upload') {
        $image_name = $_FILES['files']['name'][$key];
        $validators = array(
           'file_validate_extensions' => array('jpg jpeg png gif'),
        );
        $image_file = file_save_upload($key, $validators, $dir);
        $image_file->status = FILE_STATUS_PERMANENT;
        file_save($image_file);
        if ($image_file) {
          $pos = drupal_strlen($key) - strrpos($key, '_') - 1;
          $index = drupal_substr($key, -$pos);
          $upload_event[$index] = base_path() . 'sites/default/files/' . $image_name . '#***#';
        }
      }
    }
  }
  // Array to string.
  if (!isset($items)) {
    $items = '';
  }
  // Add to files list old uploaded files.
  foreach ($_POST as $key => $value) {
    if (drupal_substr($key, 0, 10) == 'delete_img') {
      $items .= $value;
    }
  }
  // Add to files list new uploaded files.
  foreach ($upload_event as $upload_event_value) {
    $items .= $upload_event_value;
  }
  $contact_description = $form_state['values']['contact_description'];
  $contact_short_description = $form_state['values']['contact_short_description'];
  if (isset($_POST['images'])) {
    $images_array = check_plain($_POST['images']);
  }
  else {
    $images_array = '';
  }
  $images_array .= $items;
  $params = db_query("SELECT param FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $contact_category_id))->fetchField();
  $params_array = explode('#***#', $params);
  $parameters_array = '';
  foreach ($params_array as $key => $param_name) {
    if (isset($_POST['param_' . $key]) && ($param_name != '')) {
      $parameters_array .= $param_name . '@@:@@' . check_plain($_POST['param_' . $key]) . '#@@@#';
    }
  }
  $contact_published = $form_state['values']['contact_published'];

  if (isset($_GET['contact_id']) && ($_GET['contact_id'] != '')) {
    $contact_id = check_plain($_GET['contact_id']);
    db_query("UPDATE {spider_contacts_contacts} SET 
      first_name=:first_name,
      last_name=:last_name,
      category_id=:category_id,
      email=:email,
      want_email=:want_email,
      image_url=:image_url,
      description=:description,
      short_description=:short_description,
      param=:param,
      published=:published WHERE id=:id", array(
      ':first_name' => $contact_first_name,
      ':last_name' => $contact_last_name,
      ':category_id' => $contact_category_id,
      ':email' => $contact_email,
      ':want_email' => $contact_want_email,
      ':image_url' => $images_array,
      ':description' => $contact_description,
      ':short_description' => $contact_short_description,
      ':param' => $parameters_array,
      ':published' => $contact_published,
      ':id' => $contact_id));
  }
  else {
    db_insert('spider_contacts_contacts')
      ->fields(array(
        'first_name' => $contact_first_name,
        'last_name' => $contact_last_name,
        'category_id' => $contact_category_id,
        'email' => $contact_email,
        'want_email' => $contact_want_email,
        'image_url' => $images_array,
        'description' => $contact_description,
        'short_description' => $contact_short_description,
        'param' => $parameters_array,
        'ordering' => 0,
        'published' => $contact_published,
        ))
      ->execute();
  }
  drupal_set_message(t('Your contact successfully saved.'), 'status', FALSE);
  $form_state['redirect'] = url('admin/settings/spider_contacts/contacts', array('absolute' => TRUE));
}

/**
 * Apply contact.
 */
function spider_contacts_contact_apply($form, &$form_state) {
  $contact_first_name = $form_state['values']['contact_first_name'];
  $contact_last_name = $form_state['values']['contact_last_name'];
  $contact_category_id = $form_state['values']['contact_categories'];
  $contact_email = $form_state['values']['contact_email'];
  $contact_want_email = $form_state['values']['contact_want_email'];
  $dir = 'public://';
  $upload_event = array();
  if ($_FILES) {
    foreach ($_FILES['files']['name'] as $key => $value) {
      if (drupal_substr($key, 0, 12) == 'image_upload') {
        $image_name = $_FILES['files']['name'][$key];
        $validators = array(
           'file_validate_extensions' => array('jpg jpeg png gif'),
        );
        $image_file = file_save_upload($key, $validators, $dir);
        $image_file->status = FILE_STATUS_PERMANENT;
        file_save($image_file);
        if ($image_file) {
          $pos = drupal_strlen($key) - strrpos($key, '_') - 1;
          $index = drupal_substr($key, -$pos);
          $upload_event[$index] = base_path() . 'sites/default/files/' . $image_name . '#***#';
        }
      }
    }
  }
  // Array to string.
  if (!isset($items)) {
    $items = '';
  }
  // Add to files list old uploaded files.
  foreach ($_POST as $key => $value) {
    if (drupal_substr($key, 0, 10) == 'delete_img') {
      $items .= $value;
    }
  }
  // Add to files list new uploaded files.
  foreach ($upload_event as $upload_event_value) {
    $items .= $upload_event_value;
  }
  $contact_description = $form_state['values']['contact_description'];
  $contact_short_description = $form_state['values']['contact_short_description'];
  if (isset($_POST['images'])) {
    $images_array = check_plain($_POST['images']);
  }
  else {
    $images_array = '';
  }
  $images_array .= $items;
  $params = db_query("SELECT param FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $contact_category_id))->fetchField();
  $params_array = explode('#***#', $params);
  $parameters_array = '';
  foreach ($params_array as $key => $param_name) {
    if (isset($_POST['param_' . $key]) && ($param_name != '')) {
      $parameters_array .= $param_name . '@@:@@' . check_plain($_POST['param_' . $key]) . '#@@@#';
    }
  }
  $contact_published = $form_state['values']['contact_published'];

  if (isset($_GET['contact_id']) && ($_GET['contact_id'] != '')) {
    $contact_id = check_plain($_GET['contact_id']);
    db_query("UPDATE {spider_contacts_contacts} SET 
      first_name=:first_name,
      last_name=:last_name,
      category_id=:category_id,
      email=:email,
      want_email=:want_email,
      image_url=:image_url,
      description=:description,
      short_description=:short_description,
      param=:param,
      published=:published WHERE id=:id", array(
      ':first_name' => $contact_first_name,
      ':last_name' => $contact_last_name,
      ':category_id' => $contact_category_id,
      ':email' => $contact_email,
      ':want_email' => $contact_want_email,
      ':image_url' => $images_array,
      ':description' => $contact_description,
      ':short_description' => $contact_short_description,
      ':param' => $parameters_array,
      ':published' => $contact_published,
      ':id' => $contact_id));
    drupal_set_message(t('Your contact successfully updated.'), 'status', FALSE);
  }
  else {
    db_insert('spider_contacts_contacts')
      ->fields(array(
        'first_name' => $contact_first_name,
        'last_name' => $contact_last_name,
        'category_id' => $contact_category_id,
        'email' => $contact_email,
        'want_email' => $contact_want_email,
        'image_url' => $images_array,
        'description' => $contact_description,
        'short_description' => $contact_short_description,
        'param' => $parameters_array,
        'ordering' => 0,
        'published' => $contact_published,
        ))
      ->execute();
    $contact_id = db_query("SELECT MAX(id) FROM {spider_contacts_contacts}")->fetchField();
    drupal_set_message(t('Your contact successfully saved.'), 'status', FALSE);
  }
  $form_state['redirect'] = url('admin/settings/spider_contacts/contacts/edit', array('query' => array('contact_id' => $contact_id), 'absolute' => TRUE));
}

/**
 * Cancel contact save.
 */
function spider_contacts_contact_cancel($form, &$form_state) {
  $form_state['redirect'] = url('admin/settings/spider_contacts/contacts', array('absolute' => TRUE));
}
