<?php
/**
 * @package Spider Contacts
 * @author Web-Dorado
 * @copyright (C) 2012 Web-Dorado. All rights reserved.
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

/**
 * Fill CSV.
 */
function spider_contacts_csv($fields) {
  $pattern = "/([a-zA-Z0-9\-]*)@@:@@(([a-zA-Z0-9\-(),.\+_]|\x20)*)/";
  $separator = '';
  foreach ($fields as $field) {
    if (substr_count($field, '@@:@@')) {
      $string = '';
      $par_data = explode('#@@@#', $field);
      for ($j = 0; $j < count($par_data); $j++)
        if ($par_data[$j] != '') {
          $par1_data = explode('@@:@@', $par_data[$j]);
          $par_values = explode('#***#', $par1_data[1]);
          $countOfPar = 0;
          for ($k = 0; $k < count($par_values); $k++)
            if ($par_values[$k] != "")
              $countOfPar++;
          if ($countOfPar != 0) {
            $string .= $par1_data[0] . ':';
            for ($k = 0; $k < count($par_values); $k++)
              if ($par_values[$k] != "")
                $string .= $par_values[$k] . ', ';
            $string .= "\n";
          }
        }
      $field = $string;
    }
    $field1 = $field;
    if (preg_match_all($pattern, $field, $res)) {
      $field1 = '';
      for ($i = 0; $i < count($res[0]); $i++) {
        $field1 .= $res[1][$i] . ':' . $res[2][$i] . ' ';
      }
    }
    if (preg_match('/\\r|\\n|,|"/', $field1)) {
      $field1 = '"' . str_replace('"', '""', $field1) . '"';
    }
    echo $separator . $field1;
    $separator = ',';
  }
  echo "\r\n";
}

/**
 * Generate CSV.
 */
function spider_contacts_export_csv() {
  $filename = 'export_' . date("d-m-y") . '.csv';
  header('Content-Description: File Transfer');
  header('Content-Type: text/csv');
  header('Content-Disposition: attachment;filename=' . $filename);
  $query = db_select('spider_contacts_contacts', 'n');
  $query->innerJoin('spider_contacts_contacts_categories', 'u', 'n.category_id = u.id');
  $query->fields('n', array('First Name' => 'first_name', 'Last Name' => 'last_name', 'Parameters' => 'param'));
  $query->fields('u', array('Category' => 'name'));
  $query->condition(
      db_or()
        ->condition('n.first_name', '%' . db_like(variable_get('spider_contacts_search_contacts_name', '')) . '%', 'LIKE')
        ->condition('n.last_name', '%' . db_like(variable_get('spider_contacts_search_contacts_name', '')) . '%', 'LIKE')
    );
  if (variable_get('spider_contacts_search_contacts_by_category', '') != '') {
    $query = $query->condition('n.category_id', variable_get('spider_contacts_search_contacts_by_category', ''), '=');
  }
  $contacts_ids = $query->execute();
  // Output header row.
  if ($contacts_ids) {
    spider_contacts_csv(array('First Name', 'Last Name', 'Parameters', 'Category'));
  }
  // Output data rows.
  foreach ($contacts_ids as $contact_id) {
    spider_contacts_csv($contact_id);
  }
}
