<?php
/**
 * @file
 * This file contains all functions which need for contacts view.
 */

/**
 * View contacts in cells.
 */
function spider_contacts_contact_view($nodeid, $theme_id, $contact_id) {
  // Parameters for SpiderBox.
  drupal_add_js(array(
    'spider_contacts' => array(
      'delay' => 3000,
      'allImagesQ' => 0,
      'slideShowQ' => 0,
      'darkBG' => 1,
      'juriroot' => base_path() . drupal_get_path('module', 'spider_contacts'),
      'spiderShop' => 1,
    ),
    ),
    'setting');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/spiderBox/spiderBox.js');
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_common.js');
  if (isset($_GET['back'])) {
    $back_url = '&back=' . check_plain($_GET['back']);
    $nodeid = check_plain($_GET['back']);
  }
  else {
    $back_url = '';
  }
  if (isset($_GET['page_num'])) {
    $page_num = check_plain($_GET['page_num']);
  }
  else {
    $page_num = 1;
  }
  $page_url = '&page_num=' . $page_num;
  if (!isset($_GET['contact_id'])) {
    $link_url = url('node/' . $nodeid, array('query' => array('example' => 1), 'absolute' => TRUE));
  }
  else {
    $contact_id = check_plain($_GET['contact_id']);
    $theme_id = (isset($_GET['theme_id']) ? check_plain($_GET['theme_id']) : 1);
    $link_url = url('spider_contacts/contact_view', array('query' => array(
      'theme_id' => $theme_id,
      'contact_id' => $contact_id,
      ), 'absolute' => TRUE)) . $back_url ;
  }
  $content = '';
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  if (!$theme) {
    $theme_id = 1;
    db_query("UPDATE {spider_contacts_form_table} SET theme=:theme WHERE vid=:vid", array(':theme' => 1, ':vid' => $nodeid));
  }
  $theme = db_query("SELECT * FROM {spider_contacts_themes} WHERE id=:id", array(':id' => $theme_id))->fetchObject();
  $row = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $contact_id))->fetchObject();
  // If contact deleted.
  if (!$row) {
    $content = '<div class="messages error">' . t('The chosen Contact was deleted. Select another Contact !url.', array('!url' => l(t('here'), url('node/' . $nodeid . '/edit', array('absolute' => TRUE))))) . '</div>';
    return $content;
  }
  $category_name = db_query("SELECT name FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $row->category_id))->fetchField();
  $params = db_query("SELECT * FROM {spider_contacts_params} WHERE id=:id", array(':id' => 1))->fetchObject();
  if ($theme->viewcontact_radius) {
    $border_radius = '8px';
  }
  else {
    $border_radius = '0';
  }
  $content .= '
    <style type="text/css">
      #paramstable td span,
      #paramstable td ul,
      #paramstable td li {
        list-style: none;
        padding: 3px 0 3px 0 !important;
      }
      .spidercontactparamslist {
        margin: 0 !important;
        padding: 0 0 0 1em !important;
      }
      #tdviewportheight {
        border: none;
      }
      #contactMainDiv tr,
      #cap_table tr {
        background-color: rgba(0,0,0,0);
      }
      #contactMainDiv tbody {
        border: none;
      }
      #contactMainDiv th {
        text-transform:none !important;
        text-align:center !important;
      }
      #contactMainDiv,
      .spidercontactbutton,
      .spidercontactinput {
        -webkit-border-radius: ' . $border_radius . ';
        -moz-border-radius: ' . $border_radius . ';
        border-radius: ' . $border_radius . ';
      }
      #contactMainDiv #contTitle {
        -webkit-border-top-right-radius: ' . $border_radius . ';
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        -moz-border-radius-topleft: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
      }
      select.input {
        margin: 0 0 24px 0 !important;
        top: -11px !important;
        position: relative;
      }
      #contactMainDiv {
        -webkit-border-radius: ' . $border_radius . ' !important;
        -moz-border-radius: ' . $border_radius . ' !important;
        border-radius: ' . $border_radius . ' !important;
      }
      .spidercontactbutton,
      .input {
        font-size:10px !important;
        -webkit-border-radius: ' . $border_radius . ' !important;
        -moz-border-radius: ' . $border_radius . ' !important;
        border-radius: ' . $border_radius . ' !important;
        padding: 4px;
      }
      #contactMainDiv td,
      #contactMainDiv tr,
      #contactMainDiv tbody,
      #contactMainDiv div {
        line-height:inherit !important;
        color:inherit;
        opacity:inherit !important;        
      }
      #contactMainDiv {
        min-width:540px;
      }
      #contactMainDiv #contTitle {
        font-size:10px;
        -webkit-border-top-right-radius: ' . $border_radius . ';
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        -moz-border-radius-topleft: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
      }
      #spider_contact_top_table table,
      #spider_contact_top_table th,
      #spider_contact_top_table tbody,
      #spider_contact_top_table tr,
      #spider_contact_top_table td,
      #spider_contact_top_table body {
        vertical-align:middle;
        line-height:inherit;
        font-weight:bold;
        text-align:left;
      }
      #spider_contact_top_table {
        border-collapse:inherit;
        padding:inherit;
        margin:inherit;
        border:inherit !important;
      }
      #spider_contact_top_table ul,
      #spider_contact_top_table li,
      #spider_contact_top_table a {
        list-style:inherit;
      }
      #contMiddle tr td,
      #contMiddle td,
      #contMiddle tr,
      #contMiddle table,
      #contMiddle table,
      #contMiddle {
        border:0 !important;
      }
      #contMiddle td {
        padding: 0 2px;
      }
      #paramstable ul,
      #paramstable li {
        width:inherit !important;
        float:left;
        overflow-x:inherit !important;
        overflow-y:inherit !important;
      }
      #paramstable {
        margin:0px !important;
      }
      #contAllDescription p{
        line-height: inherit;
        margin: 0;
        padding: 5px;
      }
      #paramstable,
      #paramstable table,
      #paramstable tbody {
        width:inherit !important;
        float:right;
        border-collapse:inherit;
      }
      #paramstable td {
        text-align:left;
      }
      #tdviewportheight div,
      #tdviewportheight td,
      #tdviewportheight {
        vertical-align:middle;
      }
      #spider_contact_image_table {
        border-spacing:inherit;
      }
      #contTitle {
        font-family:inherit;
      }
      #message_div  input,
      #message_div textarea {
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -webkit-transition: all 0.2s linear;
        -webkit-transition-delay: 0s;
        -moz-transition: all 0.2s linear 0s;
        -o-transition: all 0.2s linear 0s;
        transition: all 0.2s linear 0s;
      }
      #cont_pref0 {
        position:relative;
        top:33px;
      }
      #cont_pref1 {
        position:relative;
        top:33px;
      }
      #cont_pref2 {
        position:relative;
        top:33px;
      }
      #message_div textarea,
      #message_div input[type="text"],
      #message_div input[type="email"] {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-background-clip: padding;
        -moz-background-clip: padding;
        background-clip: padding-box;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-appearance: none;
        background-color: white;
        border: 1px solid #cccccc;
        color: black;
        outline: 0;
        margin: 0;
        padding: 3px 4px;
        text-align: left;
        font-size: 13px;
        font-family: Arial, "Liberation Sans", FreeSans, sans-serif;
        height: 2.2em;
        vertical-align: top;
        *padding-top: 2px;
        *padding-bottom: 1px;
        *height: auto;
      }
      #message_div textarea[disabled],
      #message_div input[type="text"][disabled],
      #message_div input[type="email"][disabled] {
        background-color: #eeeeee;
      }
      #message_div textarea:focus,
      #message_div input[type="text"]:focus ,
      #message_div input[type="email"]:focus {
        border-color: rgba(82, 168, 236, 0.8);
        outline: 0;
        outline: thin dotted \9;
        /* IE6-9 */
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 ' . $border_radius . ' rgba(82, 168, 236, 0.6);
      }
      #message_div input[disabled],
      #message_div textarea[disabled] {
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        -moz-user-select: -moz-none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        user-select: none;
        color: #888888;
        cursor: default;
      }
      #message_div input::-webkit-input-placeholder,
      #message_div textarea::-webkit-input-placeholder {
        color: #888888;
      }
      #message_div input:-moz-placeholder,
      #message_div textarea:-moz-placeholder {
        color: #888888;
      }
      #message_div input.placeholder_text,
      #message_div textarea.placeholder_text {
        color: #888888;
      }
      #spider_contact_top_table td{
        border:none !important;
      }
      #message_div textarea {
        min-height: 80px;
        overflow: auto;
        padding: 5px;
        resize: vertical;
        width: 100%;
      }
      #message_div optgroup {
        color: black;
        font-style: normal;
        font-weight: normal;
        font-family: Arial, "Liberation Sans", FreeSans, sans-serif;
      }
      #message_div optgroup::-moz-focus-inner {
        border: 0;
        padding: 0;
      }
      .rating_stars ul,
      .rating_stars li,
      .rating_stars ul li{
        list-style-type:none !important;
      }
      #cap_table span,
      #cap_table,
      #cap_table tr,
      #cap_table td,
      #cap_table img,
      #cap_table tr td {
        vertical-align:middle !important;
        padding:2px;
        border:0 !important;
      }
      #message_text {
        min-height:100px;
        max-width:inherit !important;
      }
      #full_name {
        max-width:inherit !important;
      }
      #contactMainDiv,
      .spidercontactbutton,
      .spidercontactinput {
        -moz-border-radius: ' . $border_radius . ';
        -webkit-border-radius: ' . $border_radius . ';
        border-radius: ' . $border_radius . ';
      }
      #contactMainDiv #contTitle {
        -moz-border-radius-topleft: ' . $border_radius . ';
        -moz-border-radius-topright: ' . $border_radius . ';
        -webkit-border-top-left-radius: ' . $border_radius . ';
        -webkit-border-top-right-radius: ' . $border_radius . ';
        border-top-left-radius: ' . $border_radius . ';
        border-top-right-radius: ' . $border_radius . ';
      }
    </style>';
  $content .= '
    <div>';
  if ($theme->viewcontact_radius == 0) {
    $border_radius = '';
  }
  else {
    $border_radius = ' border-radius:8px; -moz-border-radius: 8px; -webkit-border-radius: 8px;';
  }
  // foreach ($rows as $row) {
    if ($back_url != '') {
      $content .= '
      <span id="back_to_spidercontact_button">
        <a href="' . url('node/' . $nodeid, array('query' => array('example' => 1), 'absolute' => TRUE)) . $page_url . '" >' . t('Back to contact') . '</a>
      </span>';
    }
    $content .= '
      <div id="contactMainDiv" style=" ' . $border_radius . ' border-width:' . $theme->viewcontact_border_width . 'px;border-color:#' . $theme->viewcontact_border_color . ';border-style:' . $theme->viewcontact_border_style . ';' . (($theme->text_size_big != '') ? ('font-size:' . $theme->text_size_big . 'px;') : '') . (($theme->viewcontact_text_color != '') ? ('color:#' . $theme->viewcontact_text_color . ';') : '') . (($theme->viewcontact_background_color != '') ? ('background-color:#' . $theme->viewcontact_background_color . ';') : '') . '">';
    $content .= '
        <div id="contTitle" style="' . (($theme->viewcontact_title_color != '') ? ('color:#' . $theme->viewcontact_title_color . ';') : '') . (($theme->viewcontact_title_background_color != '') ? ('background-color:#' . $theme->viewcontact_title_background_color . ';') : '') . 'padding:0px;">
          <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
              <td style="padding:10px;font-size:' . $theme->title_size_big . 'px;">' . $row->first_name . ' ' . $row->last_name . '</td>
            </tr>
          </table>
        </div>
        <br />
        <table id="contMiddle" border="0" cellspacing="0" cellpadding="0" style="width:100% !important;">
          <tr>
            <td valign="top" >
              <table cellpadding="0" cellspacing="5" border="0" style="margin:0px;width:' . $theme->large_picture_width . 'px;">';
    $imgurl = explode('#***#', $row->image_url);
    if (($row->image_url == '') || ($row->image_url == '#***#')) {
      $imgurl[0] = base_path() . drupal_get_path('module', 'spider_contacts') . '/images/no_image.jpg';
      $content .= '
                <tr>
                  <td colspan="2" id="cont_main_picture_container" valign="top">
                    <div style="width:' . $theme->large_picture_width . 'px;border: #CCCCCC solid 2px;padding:5px;background-color:white;">
                      <div id="cont_main_picture" style=\'width:' . $theme->large_picture_width . 'px;height:' . $theme->large_picture_height . 'px; background:url("' . $imgurl[0] . '") center no-repeat;background-size:contain;\'>&nbsp;</div>
                    </div>
                  </td>
                </tr>';
    }
    else {
      $content .= '
                <tr>
                  <td colspan="2" id="cont_main_picture_container" valign="top">
                    <div style="width:' . $theme->large_picture_width . 'px;border: #CCCCCC solid 2px;padding:5px;background-color:white;">
                      <a href="' . $imgurl[0] . '" target="_blank" id="cont_main_picture_a" style="text-decoration:none;">
                        <div id="cont_main_picture" style=\'width:' . $theme->large_picture_width . 'px;height:' . $theme->large_picture_height . 'px; background:url("' . $imgurl[0] . '") center no-repeat;background-size:contain;\'>&nbsp;</div>
                      </a>
                    </div>
                  </td>
                </tr>';
    }
    $content .= '
                <tr>
                  <td style="text-align:left;">';
    $small_images_str = '';
    $small_images_count = 0;
    foreach ($imgurl as $img) {
      if ($img !== '') {
        $small_images_str .= '<a href="' . $img . '" target="_blank">
                                <img style="width:' . $theme->small_pic_size . 'px;heigth:' . $theme->small_pic_size . 'px;" src="' . $img . '" vspace="5" hspace="5" onMouseOver=\'spider_contacts_prod_change_picture("' . $img . '",this,' . $theme->large_picture_width . ',' . $theme->large_picture_height . ');\' />
                              </a>';
        $small_images_count++;
      }
    }
    if ($small_images_count > 1) {
      $content .= $small_images_str;
    }
    else {
      $content .= '&nbsp;';
    }
    $content .= ' </td>
                </tr>
                <tr>
                  <td valign="top"></td>
                </tr>
              </table>
            </td>
            <td valign="top" align="right" style="width:100% !important;">
              <table border="1" style="margin:0;' . $border_radius . ' border-style:solid; border-color:' . $theme->viewcontact_border_color . ';">
                <tr>
                  <td style="border:none;">
                    <table ' . (($theme->viewcontact_background_color != '') ? ('bordercolor="#' . $theme->viewcontact_background_color . '"') : ' ') . ' id="paramstable" style="' . $border_radius . ' border-style:solid;width:100% !important;" border="1" cellspacing="0" cellpadding="5">';
    if ($category_name != '') {
      $content .= ' <tr valign="top" style="' . (($theme->viewcontact_params_background_color2 != '') ? ('background-color:#' . $theme->viewcontact_params_background_color2 . ';') : '') . ' vertical-align:middle;">
                      <td>
                        <b>' . t('Category') . '</b>
                      </td>
                      <td style="width:100%;' . (($theme->viewcontact_params_color != '') ? ('color:#' . $theme->viewcontact_params_color . ';') : '') . '">
                        <span id="cat_' . $row->id . '">' . $category_name . '</span>
                      </td>
                    </tr>';
    }
    else {
      $content .= '<span id="cat_' . $row->id . '"></span>';
    }
    if ($row->email != '') {
      $content .= ' <tr valign="top" style=" ' . (($theme->viewcontact_params_background_color1 != '') ? ('background-color:#' . $theme->viewcontact_params_background_color1 . ';') : '') . ' vertical-align:middle;">
                      <td>
                        <b>' . t('Email') . '</b>
                      </td>
                      <td style="width:100%;' . (($theme->params_color != '') ? ('color:#' . $theme->viewcontact_params_color . ';') : '') . '">
                        <span id="email_' . $row->id . '">
                          <a href="mailto:' . $row->email . '">' . $row->email . '</a>
                        </span>
                      </td>
                    </tr>';
    }
    else {
      $content .= '<span id="email_' . $row->id . '"></span>';
    }
    $par_data = explode("#@@@#",$row->param);
    for ($j = 0; $j < count($par_data); $j++) {
      if ($par_data[$j] != '') {
        $par1_data = explode("@@:@@", $par_data[$j]);
        $par_values = explode("#***#", $par1_data[1]);
        $countOfPar = 0;
        for ($k = 0; $k < count($par_values); $k++) {
          if ($par_values[$k] != "") {
            $countOfPar++;
          }
        }
        $bgcolor = (($j % 2) ? (($theme->viewcontact_params_background_color2 != '') ? ('background-color:#' . $theme->viewcontact_params_background_color2 . ';') : '') : (($theme->viewcontact_params_background_color1 != '') ? ('background-color:#' . $theme->viewcontact_params_background_color1 . ';') : ''));	
        if ($countOfPar != 0) {
          $content .= '
                    <tr valign="top" style="' . $bgcolor . 'text-align:left">
                      <td>
                        <b>' . $par1_data[0] . ':</b>
                      </td>
                      <td style="width:100%;' . (($theme->text_size_big != '') ? ('font-size:' . $theme->text_size_big . 'px;') : '') . $bgcolor . (($theme->viewcontact_params_color != '') ? ('color:#' . $theme->viewcontact_params_color . ';') : '') . '">
                        <ul class="spidercontactparamslist">';
          for ($k = 0; $k < count($par_values); $k++) {
            if ($par_values[$k] != "") {
              if (preg_match('/(http:\/\/[^\s]+)/', $par_values[$k], $text)) {
                $hypertext = "<a target=\"_blank\" href=\"" . $text[0] . "\">" . $text[0] . "</a>";
                $newString = preg_replace('/(http:\/\/[^\s]+)/', $hypertext, $par_values[$k]);
                $content .= '
                          <li>' . $newString . '</li><br />';
              }
							elseif (preg_match('/(https:\/\/[^\s]+)/', $par_values[$k], $text)) {
                $hypertext = "<a target=\"_blank\" href=\"" . $text[0] . "\">" . $text[0] . "</a>";
                $newString = preg_replace('/(https:\/\/[^\s]+)/', $hypertext, $par_values[$k]);
                $content .= '
                          <li>' . $newString . '</li><br />';
              }
              elseif ($par_values[$k]) {
                $content .= '
                          <li>' . $par_values[$k] . '</li><br />';
              }
						}
          }
          $content .= ' </ul>
                      </td>
                    </tr>';
        }
      }
    }
    $content .= '   </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br />
        <div style="' . (($theme->description_text_color != '') ? ('color:#' . $theme->description_text_color . ';') : '') . '" id="contAllDescription">' . $row->description . '</div>
        <br /><br />
      </div>';
	// }
  if ($params->enable_message) {
    $content .= '
      <div>
        <a name="mes" style="color:inherit;text-decoration:inherit;">' . t('Add message') . '</a>
      </div>';
    $a = 'form';
    $content .= '
      <div id="message_div" style="' . $border_radius . ' padding:10px; border-width:' . $theme->viewcontact_border_width . 'px;border-color:#' . $theme->viewcontact_border_color . ';border-style:' . $theme->viewcontact_border_style . ';' . (($theme->messages_background_color != '') ? ('background-color:#' . $theme->messages_background_color . ';') : '') . '">
        <' . $a . ' action="' . $link_url . '"  name="message" method="post" >';
    if ($params->show_name) {
      $content .= '
          <div style="font-weight:bold;">' . t('Your name') . '</div>
          <input type="text" name="full_name" id="full_name" style="width:98%; margin:0px;" />
          <br /><br />';
    }
    else {
      $content .= '
          <input type="hidden" name="full_name" id="full_name" value="No Name" /> ';
    }
    if ($params->show_phone) {
      $content .= '
          <div style="font-weight:bold;">' . t('Phone') . '</div>
          <input type="text" name="phone" id="phone" style="width:98%; margin:0px;" />
          <br /><br />';
		}
    else {
      $content .= '
          <input type="hidden" name="phone" id="phone" value="No Phone" />';
    }
    if ($params->show_email) {
      $content .= '
          <div style="font-weight:bold;">' . t('Mail') . '</div>
          <input type="email" name="email" id="email" style="width:98%; margin:0px;" />
          <br /><br />';
    }
    else {
      $content .= '
          <input type="hidden" name="email" id="email" value="No Email"/>';
    }
    if ($params->show_cont_pref && $params->show_email && $params->show_phone) {
      $content .= '
          <div style="font-weight:bold;">' . t('Contact Preference') . '</div>
          <input type="radio" name="cont_pref" id="cont_pref1" value="' . t('Phone') . '" style="width:98%; margin:0px;" />
          <label for="cont_pref1">' . t('Phone') . '</label>
          <input type="radio" name="cont_pref" id="cont_pref0" value="' . t('Mail') . '" checked="checked" style="width:98%; margin:0px;" />
          <label for="cont_pref0">' . t('Mail') . '</label>
          <input type="radio" name="cont_pref" id="cont_pref2" value="' . t('Either') . '" style="width:98%; margin:0px;" />
          <label for="cont_pref2">' . t('Either') . '</label>
          <br /><br />';
    }
    else {
      $content .= '
          <input type="hidden" name="cont_pref" id="cont_pref" value="Disabled"/>';
    }
    $content .= '
          <div style="font-weight:bold;">' . t('Title') . '</div>
          <input type="text" name="mes_title" id="mes_title" style="width:98%; margin:0px;" />
          <br /><br />
          <div style="font-weight:bold;">' . t('Message') . '</div>
          <textarea rows="4" name="message_text" id="message_text" style="width:98%; margin:0px;"></textarea>
          <input type="hidden" name="want_email" value="' . $row->want_email . '" />
          <input type="hidden" name="contact_id" value="' . $row->id . '" />
          <input type="hidden" name="view" value="showcontact" />
          <input type="hidden" name="is_message" value=true />
          <br /><br />
          <table id="cap_table" cellpadding="0" cellspacing="10" border="0" valign="middle" width="100%">
            <tr>
              <td>' . t('Enter the code') . '</td>
              <td>
                <span id="wd_captcha">
                  <img src="' . url('spider_contacts/captcha', array('query' => array('ex' => 1), 'absolute' => TRUE)) . '" id="wd_captcha_img" height="24" width="80" />
                </span>
                <a href="javascript:spider_contacts_refreshCaptcha();" style="text-decoration:none">&nbsp;
                  <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/refresh.png" border="0" style="border:none" />
                </a>&nbsp;
              </td>
              <td>
                <input type="text" name="code" id="message_capcode" size="6" />
                <span id="caphid" style="display:none;"></span>
              </td>
              <td width="40%" align="right">
                <input type="button" class="spidercontactbutton" style="background-color:#' . $theme->full_button_background_color . '; color:#' . $theme->full_button_color . ';width:inherit;margin-right:10px;" value="' . t('Send') . '" onclick="spider_contacts_submit_message();" />
              </td>
            </tr>
          </table>
        </form>
      </div>';
    if (isset($_POST['code'])) {
      $code = $_POST['code'];
    }
    else {
      $code = '';
    }
  }
  $content .= '
      <br /><br />
    </div>';
  drupal_add_js('var SpiderCatOFOnLoad = window.onload; window.onload = SpiderCatAddToOnload;', array('type' => 'inline', 'scope' => 'footer'));
  $full_name = ((isset($_POST['full_name'])) ? check_plain($_POST['full_name']) : '');
  $phone = ((isset($_POST['phone'])) ? check_plain($_POST['phone']) : '');
  $email = ((isset($_POST['email'])) ? check_plain($_POST['email']) : '');
  $cont_pref = ((isset($_POST['cont_pref'])) ? check_plain($_POST['cont_pref']) : '');
  $message_title = ((isset($_POST['mes_title'])) ? check_plain($_POST['mes_title']) : '');
  $message_text = ((isset($_POST['message_text'])) ? check_plain($_POST['message_text']) : '');
  $code = ((isset($_POST['code'])) ? check_plain($_POST['code']) : '');
  $code_come_in_sesssion = ((isset($_SESSION['spider_contacts_captcha_code'])) ? $_SESSION['spider_contacts_captcha_code'] : '');
  if (($code != '') && ($full_name != '') && ($code == $code_come_in_sesssion)) {
    $save = db_insert('spider_contacts_messages')
      ->fields(array(
        'sender' => $full_name,
        'text' => $message_text,
        'to_contact' => $row->id,
        'title' => $message_title, 
        'email' => $email, 
        'phone' => $phone, 
        'cont_pref' => $cont_pref, 
        'date' => date('Y-m-d H:i'),
        'readen' => 0,
      ))
      ->execute();
    if (($row->want_email != 0) && ($row->email != '')) {
      if ($email != '') {
        $from = $email;
        $attachments = array();
        $body_style = 'background-color:#BEC8D1; text-align:right; width:30%; color:#666; font-weight:bold; border-bottom:1px solid #e9e9e9; border-right:1px solid #e9e9e9; padding:3px;';
        $body = '
          <span>' . t('A Message from') . ' <u style="font-size:150%;" >' . $full_name . '</u></span>
          <table>
            <tr>
              <td class="paramlist_key"><span class="editlinktip">&nbsp;</span></td>
              <td class="paramlist_value"><b>' . t('Sender Details') . '</b></td>
            </tr>
            <tr>
              <td style="' . $body_style . '" width="100" align="right">' . t('Sender Phone') . ':</td>
              <td style="padding:3px;">' . $phone . '</td>
            </tr>
            <tr>
              <td style="' . $body_style . '" width="100" align="right">' . t('Sender Email') . ':</td>
              <td style="padding:3px;">' . $email . '</td>
            </tr>
            <tr>
              <td style="' . $body_style . '" width="100" align="right">' . t('Sender Contact Preference') . ':</td>
              <td style="padding:3px;">' . $cont_pref . '</td>
            </tr>
            <tr>
              <td style="' . $body_style . '" width="40%" class="paramlist_key"><span class="editlinktip">&nbsp;</span></td>
              <td style="padding:3px;" class="paramlist_value"><b>' . t('Message Details') . '</b></td>
            </tr>
            <tr>
              <td style="' . $body_style . '" width="100" align="right">' . t('To Contact') . ':</td>
              <td style="padding:3px;">' . $row->email . '</td>
            </tr>
            <tr>
              <td style="' . $body_style . '" width="100" align="right">' . t('Category') . ':</td>
              <td style="padding:3px;">' . $category_name . '</td>
            </tr>
            <tr>
              <td style="' . $body_style . '" width="100" align="right">' . t('Title') . ':</td>
              <td style="padding: 3px;">' . $message_title . '</td>
            </tr>
            <tr>
              <td valign="top" style="' . $body_style . '" width="100" align="right">' . t('Message') . ':</td>
              <td style="padding:3px;">' . $message_text . '</td>
            </tr>
          </table>';
        $send = new spider_contacts_Attachmentemail($row->email, $from, $full_name, $body, $attachments);
        $send->send();
        if ($send) {
          drupal_set_message(t('Message Sent Successfully.'), 'status', FALSE);
        }
        else {
          drupal_set_message(t('Message sent but not emailed.'), 'status', FALSE);
        }
      }
    }
    elseif ($save) {
      drupal_set_message(t('Message Sent.'), 'status', FALSE);
    }
    else {
      drupal_set_message(t('Message was not sent.'), 'error', FALSE);
    }
  }
  return $content;
}

/**
 * Send the email.
 */
class spider_contacts_Attachmentemail {
  protected $to = '';
  protected $from = '';
  protected $subject = '';
  protected $message = '';
  protected $attachedfiles = '';

  /**
   * Constructor.
   */
  public function __construct($to, $from, $subject, $message, $attachedfiles = array()) {
    $this->to = $to;
    $this->from = $from;
    $this->subject = $subject;
    $this->message = $message;
    $this->attachedfiles = $attachedfiles;
    $this->boundary = md5(date('r', time()));
  }

  /**
   * Send the email.
   */
  public function send() {
    $header = "From: " . ($this->from) . " <" . ($this->from) . ">" . PHP_EOL;
    $header .= "Reply-To: " . ($this->from) . PHP_EOL;

    // $header .= "Content-Transfer-Encoding: 7bit" . PHP_EOL;
    $header .= "Content-Type: multipart/mixed; boundary=\"" . $this->boundary . '"' . PHP_EOL;
    $header .= "MIME-Version: 1.0" . PHP_EOL;
    $message = "This is a multi-part message in MIME format." . PHP_EOL . PHP_EOL;
    $message .= "--" . $this->boundary . PHP_EOL;
    // $message .= "Content-Transfer-Encoding: binary" . PHP_EOL;
    $message .= "Content-type:text/html; charset=UTF-8; format=flowed;" . PHP_EOL . PHP_EOL;
    $message .= $this->message . PHP_EOL . PHP_EOL;
    $message .= $this->getbinaryattachments() . PHP_EOL;
    $message .= '--' . $this->boundary . '--' . PHP_EOL;
    $this->message = $message;

    if (mail($this->to, $this->subject, $this->message, $header)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the attachments in a base64 format.
   */
  public function getbinaryattachments() {
    $output = '';

    foreach ($this->attachedfiles as $attachment) {
      $info = pathinfo($attachment);
      switch ($info['extension']) {
        case 'jpg':
        case 'jpe':
        case 'jpeg':
        case 'bmp':
        case 'gif':
          $mime = 'image/jpeg jpg';
          break;

        case 'xls':
        case 'xlsx':
          $mime = 'application/msexcel';
          break;

        case 'png':
          $mime = 'image/png';
          break;

        case 'doc':
        case 'docx':
          $mime = 'application/msword';
          break;

        case 'txt':
          $mime = 'text/plain';
          break;

        case 'zip':
          $mime = 'application/zip';
          break;

        case 'avi':
          $mime = 'video/x-msvideo';
          break;

        case 'htm':
        case 'html':
          $mime = 'text/html';
          break;

        case 'mp3':
          $mime = 'audio/mpeg';
          break;

        case 'mpa':
        case 'mpe':
        case 'mpeg':
        case 'mpg':
          $mime = 'video/mpeg';
          break;

        default:
          $mime = 'image/jpeg jpg';
          break;
      }
      $file = array(
        'path' => $attachment,
        'filename' => $info['basename'],
        'type' => $mime,
      );
      $attachment_bin = file_get_contents($file['path']);
      $attachment_bin = chunk_split(base64_encode($attachment_bin));
      $output .= '--' . $this->boundary . PHP_EOL;
      $output .= 'Content-Type: ' . $file['type'] . '; name="' . basename($file['path']) . '"' . PHP_EOL;
      $output .= 'Content-Transfer-Encoding: base64' . PHP_EOL;
      $output .= 'Content-Disposition: attachment' . PHP_EOL . PHP_EOL;
      $output .= $attachment_bin . PHP_EOL . PHP_EOL;
    }
    return $output;
  }
}
