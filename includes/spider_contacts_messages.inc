<?php
/**
 * @file
 * Spider Contacts module Contacts messages.
 */

/**
 * Menu loader callback. Load Contacts messages table.
 */
function spider_contacts_messages() {
  drupal_add_js(drupal_get_path('module', 'spider_contacts') . '/js/spider_contacts_cal.js');
  drupal_add_css(drupal_get_path('module', 'spider_contacts') . '/css/spider_contacts_cal.css');
  $form = array();
  $free_version = '<a href="http://web-dorado.com/drupal-contacts-guide-step-4.html" target="_blank" style="float:right;"><img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/user-manual.png" border="0" alt="' . t('User Manual') . '"></a><div style="clear:both;"></div>
  <a href="http://web-dorado.com/products/drupal-contacts-module.html" target="_blank" style="color:red; text-decoration:none; float:right;">
                    <img src="' . base_path() . drupal_get_path('module', 'spider_contacts') . '/images/header.png" border="0" alt="www.web-dorado.com" width="215"><br />
                  <div style="float:right;">' . t('Get the full version') . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
                  </a>';
  $form['fieldset_message_buttons'] = array(
    '#prefix' => $free_version,
    '#type' => 'fieldset',
  );
  $form['fieldset_message_buttons']['readen_message'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_messages_readen'),
    '#value' => t('Mark as Read'),
  );
  $form['fieldset_message_buttons']['unreaden_message'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_messages_unreaden'),
    '#value' => t('Mark as Unread'),
  );
  $form['fieldset_message_buttons']['delete_message'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_messages_delete'),
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'if (!confirm(Drupal.t("Do you want to delete selected messages?"))) {return false;}'),
  );
  // Search messages by title.
  $form['fieldset_search_messages_name'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search messages by title'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_contacts_search_message_title', '') == '') ? TRUE : FALSE),
  );
  $form['fieldset_search_messages_name']['search_messages_name'] = array(
    '#type' => 'textfield',
    '#size' => 25,
    '#default_value' => variable_get('spider_contacts_search_message_title', ''),
  );
  $form['fieldset_search_messages_name']['search_messages'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_search_messages_name'),
    '#value' => t('Go'),
  );
  $form['fieldset_search_messages_name']['reset_messages'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#submit' => array('spider_contacts_reset_messages_name'),
  );
  // Search messages by date.
  $form['fieldset_search_messages_date'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search messages by date'),
    '#collapsible' => TRUE,
    '#collapsed' => (((variable_get('spider_contacts_search_message_date_from', '') == '') && (variable_get('spider_contacts_search_message_date_to', '') == '')) ? TRUE : FALSE),
  );
  $form['fieldset_search_messages_date']['search_messages_date_from'] = array(
    '#type' => 'textfield',
    '#title' => t('From:'),
    '#size' => 15,
    '#default_value' => variable_get('spider_contacts_search_message_date_from', ''),
    '#attributes' => array(
      'id' => 'search_messages_date_from',
    ),
  );
  $form['fieldset_search_messages_date']['data_search_from_button'] = array(
    '#type' => 'submit',
    '#value' => t('...'),
    '#attributes' => array(
      'onclick' => 'return showCalendar("search_messages_date_from","%Y-%m-%d");',
      'class' => array('data_search_from_button'),
    ),
  );
  $form['fieldset_search_messages_date']['search_messages_date_to'] = array(
    '#type' => 'textfield',
    '#title' => t('To:'),
    '#size' => 15,
    '#default_value' => variable_get('spider_contacts_search_message_date_to', ''),
    '#attributes' => array(
      'id' => 'search_messages_date_to',
    ),
  );
  $form['fieldset_search_messages_date']['data_search_to_button'] = array(
    '#type' => 'submit',
    '#value' => t('...'),
    '#attributes' => array(
      'onclick' => 'return showCalendar("search_messages_date_to","%Y-%m-%d");',
      'class' => array('data_search_to_button'),
    ),
  );
  $form['fieldset_search_messages_date']['search_messages_date'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_search_messages_date'),
    '#value' => t('Go '),
  );
  $form['fieldset_search_messages_date']['reset_messages_date'] = array(
    '#type' => 'submit',
    '#value' => t('Reset '),
    '#submit' => array('spider_contacts_reset_messages_date'),
  );
  // Search messages by category.
  $form['fieldset_search_messages_by_category'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search messages by category'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_contacts_search_messages_by_category', '') == '') ? TRUE : FALSE),
  );
  $form['fieldset_search_messages_by_category']['search_messages_by_category_select'] = array(
    '#type' => 'select',
    '#options' => db_query("SELECT id,name FROM {spider_contacts_contacts_categories} ORDER BY name")->fetchAllKeyed(),
    '#empty_option' => t('-Select Category-'),
    '#default_value' => variable_get('spider_contacts_search_messages_by_category', ''),
  );
  $form['fieldset_search_messages_by_category']['search_messages_by_category'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_search_messages_by_category'),
    '#value' => t(' Go'),
  );
  $form['fieldset_search_messages_by_category']['reset_messages_by_category'] = array(
    '#type' => 'submit',
    '#value' => t(' Reset'),
    '#submit' => array('spider_contacts_reset_messages_by_category'),
  );
  // Search messages by contacts.
  $form['fieldset_search_messages_by_contact'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search messages by contact'),
    '#collapsible' => TRUE,
    '#collapsed' => ((variable_get('spider_contacts_search_messages_by_contact', '') == '') ? TRUE : FALSE),
  );
  $contacts_array = array();
  $contacts = db_query("SELECT id FROM {spider_contacts_contacts}")->fetchCol();
  foreach ($contacts as $contact) {
    $contact_id = db_query("SELECT * FROM {spider_contacts_contacts} WHERE  id=:id", array(':id' => $contact))->fetchObject();
    $contacts_array[$contact_id->id] = $contact_id->first_name . ' ' . $contact_id->last_name;
  }
  $form['fieldset_search_messages_by_contact']['search_messages_by_contact_select'] = array(
    '#type' => 'select',
    '#options' => $contacts_array,
    '#empty_option' => t('-Select Contact-'),
    '#default_value' => variable_get('spider_contacts_search_messages_by_contact', ''),
  );
  $form['fieldset_search_messages_by_contact']['search_messages_by_contact'] = array(
    '#type' => 'submit',
    '#submit' => array('spider_contacts_search_messages_by_contact'),
    '#value' => t(' Go '),
  );
  $form['fieldset_search_messages_by_contact']['reset_messages_by_contact'] = array(
    '#type' => 'submit',
    '#value' => t(' Reset '),
    '#submit' => array('spider_contacts_reset_messages_by_contact'),
  );
  $header = array(
    'id' => array('data' => t('ID'), 'field' => 'n.id'),
    'title' => array('data' => t('Message Title'), 'field' => 'n.title'),
    'name' => array('data' => t('Name')),
    'sender' => array('data' => t('Sender Name'), 'field' => 'n.sender'),
    'phone' => array('data' => t('Sender Phone'), 'field' => 'n.phone'),
    'email' => array('data' => t('Sender Email'), 'field' => 'n.email'),
    'cont_pref' => array('data' => t('Contact Preferences with Sender'), 'field' => 'n.cont_pref'),
    'category' => array('data' => t('Category')),
    'date' => array('data' => t('Date'), 'field' => 'n.date'),
    'delete' => array('data' => t('Delete')),
  );
  $options = array();
  $query = db_select('spider_contacts_messages', 'n')
    ->fields('n', array('id'))
    ->condition('n.title', '%' . db_like(variable_get('spider_contacts_search_message_title', '')) . '%', 'LIKE')
    ->condition('n.date', (variable_get('spider_contacts_search_message_date_from', '') == '') ? '1900-01-01' :  variable_get('spider_contacts_search_message_date_from', ''), '>=')
    ->condition('n.date', (variable_get('spider_contacts_search_message_date_to', '') == '') ? '2200-01-01' :  variable_get('spider_contacts_search_message_date_to', ''), '<=')
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(20);
  if (variable_get('spider_contacts_search_messages_by_contact', '') != '') {
    $query = $query->condition('n.to_contact', variable_get('spider_contacts_search_messages_by_contact', ''), '=');
  }
  $messages_ids = $query->execute()->fetchCol();
  foreach ($messages_ids as $message_id) {
    $row = db_query("SELECT * FROM {spider_contacts_messages} WHERE id=:id", array(':id' => $message_id))->fetchObject();
    $contact_row = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $row->to_contact))->fetchObject();
    if ($contact_row) {
      $contact_name = $contact_row->first_name . ' ' . $contact_row->last_name;
      $category_name = db_query("SELECT name FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $contact_row->category_id))->fetchField();
    }
    else {
      $contact_name = '';
      $category_name = '';
    }
    if ((variable_get('spider_contacts_search_messages_by_category', '') == '') || ($contact_row->category_id == variable_get('spider_contacts_search_messages_by_category', ''))) {
      if ($row->readen) {
        $readen = '#FFFFFF';
      }
      else {
        $readen = '#E1E2DC';
      }
      $options[$message_id] = array(
        'id' => $message_id,
        'title' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => $row->title,
            '#href' => url('admin/settings/spider_contacts/messages/edit', array('query' => array('message_id' => $message_id), 'absolute' => TRUE)),
          ),
        ),
        'name' => $contact_name,
        'sender' => $row->sender,
        'phone' => $row->phone,
        'email' => $row->email,
        'cont_pref' => $row->cont_pref,
        'category' => $category_name,
        'date' => $row->date,
        'delete' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => t('Delete'),
            '#href' => url('admin/settings/spider_contacts/messages/delete', array('query' => array('message_id' => $message_id), 'absolute' => TRUE)),
          ),
        ),
      );
      $options[$message_id]['#attributes'] = array('style' => 'background-color:' . $readen);
    }
  }
  $form['messages_table'] = array(
    '#type' => 'tableselect',
    '#js_select' => TRUE,
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No messages available.'),
    '#suffix' => theme('pager', array('tags' => array())),
  );
  return $form;
}

/**
 * Search messages by title.
 */
function spider_contacts_search_messages_name($form, &$form_state) {
  if ($form_state['values']['search_messages_name'] != '') {
    variable_set('spider_contacts_search_message_title', $form_state['values']['search_messages_name']);
  }
  else {
    variable_set('spider_contacts_search_message_title', '');
  }
}

/**
 * Reset messages by title.
 */
function spider_contacts_reset_messages_name($form, &$form_state) {
  variable_set('spider_contacts_search_message_title', '');
}

/**
 * Search messages by date.
 */
function spider_contacts_search_messages_date($form, &$form_state) {
  if ($form_state['values']['search_messages_date_from'] != '') {
    variable_set('spider_contacts_search_message_date_from', $form_state['values']['search_messages_date_from']);
  }
  else {
    variable_set('spider_contacts_search_message_date_from', '');
  }
  if ($form_state['values']['search_messages_date_to'] != '') {
    variable_set('spider_contacts_search_message_date_to', $form_state['values']['search_messages_date_to']);
  }
  else {
    variable_set('spider_contacts_search_message_date_to', '');
  }
}

/**
 * Reset messages by date
 */
function spider_contacts_reset_messages_date($form, &$form_state) {
  variable_set('spider_contacts_search_message_date_from', '');
  variable_set('spider_contacts_search_message_date_to', '');
}

/**
 * Search messages by category.
 */
function spider_contacts_search_messages_by_category($form, &$form_state) {
  if ($form_state['values']['search_messages_by_category_select'] != '') {
    variable_set('spider_contacts_search_messages_by_category', $form_state['values']['search_messages_by_category_select']);
  }
  else {
    variable_set('spider_contacts_search_messages_by_category', '');
  }
}

/**
 * Reset messages by category
 */
function spider_contacts_reset_messages_by_category($form, &$form_state) {
  variable_set('spider_contacts_search_messages_by_category', '');
}

/**
 * Search messages by contact.
 */
function spider_contacts_search_messages_by_contact($form, &$form_state) {
  if ($form_state['values']['search_messages_by_contact_select'] != '') {
    variable_set('spider_contacts_search_messages_by_contact', $form_state['values']['search_messages_by_contact_select']);
  }
  else {
    variable_set('spider_contacts_search_messages_by_contact', '');
  }
}

/**
 * Reset messages by contact.
 */
function spider_contacts_reset_messages_by_contact($form, &$form_state) {
  variable_set('spider_contacts_search_messages_by_contact', '');
}

/**
 * Mark as read selected messages.
 */
function spider_contacts_messages_readen($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_messages}")) {
    $message_ids_col = db_query("SELECT id FROM {spider_contacts_messages}")->fetchCol();
    $flag = FALSE;
    foreach ($message_ids_col as $message_id) {
      if (isset($_POST['messages_table'][$message_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_contacts_messages} SET readen=:readen WHERE id=:id", array(':readen' => 1, ':id' => $message_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one message.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Messages successfully mark as read.'), 'status', FALSE);
    }
  }
}

/**
 * Mark as read message.
 */
function spider_contacts_message_readen() {
  if (isset($_GET['message_id'])) {
    $message_id = check_plain($_GET['message_id']);
    db_query("UPDATE {spider_contacts_messages} SET readen=:readen WHERE id=:id", array(':readen' => 1, ':id' => $message_id));
  }
  drupal_set_message(t('Message successfully mark as read.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/messages', array('absolute' => TRUE)));
}

/**
 * Mark as unread selected messages.
 */
function spider_contacts_messages_unreaden($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_messages}")) {
    $message_ids_col = db_query("SELECT id FROM {spider_contacts_messages}")->fetchCol();
    $flag = FALSE;
    foreach ($message_ids_col as $message_id) {
      if (isset($_POST['messages_table'][$message_id])) {
        $flag = TRUE;
        db_query("UPDATE {spider_contacts_messages} SET readen=:readen WHERE id=:id", array(':readen' => 0, ':id' => $message_id));
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one message.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Messages successfully mark as unread.'), 'status', FALSE);
    }
  }
}

/**
 * Mark as unread message.
 */
function spider_contacts_message_unreaden() {
  if (isset($_GET['message_id'])) {
    $message_id = check_plain($_GET['message_id']);
    db_query("UPDATE {spider_contacts_messages} SET readen=:readen WHERE id=:id", array(':readen' => 0, ':id' => $message_id));
  }
  drupal_set_message(t('Message successfully mark as unread.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/messages', array('absolute' => TRUE)));
}

/**
 * Delete selected messages.
 */
function spider_contacts_messages_delete($form, &$form_state) {
  if (db_query("SELECT id FROM {spider_contacts_messages}")) {
    $message_ids_col = db_query("SELECT id FROM {spider_contacts_messages}")->fetchCol();
    $flag = FALSE;
    foreach ($message_ids_col as $message_id) {
      if (isset($_POST['messages_table'][$message_id])) {
        $flag = TRUE;
        db_query("DELETE FROM {spider_contacts_messages} WHERE id=:id", array(':id' => $message_id));
        drupal_set_message(t('Selected messages successfully deleted.'), 'status', FALSE);
      }
    }
    if ($flag == FALSE) {
      drupal_set_message(t('You must select at least one message.'), 'warning', FALSE);
    }
    else {
      drupal_set_message(t('Messages successfully deleted.'), 'status', FALSE);
    }
  }
}

/**
 * Delete message.
 */
function spider_contacts_message_delete() {
  if (isset($_GET['message_id'])) {
    $message_id = check_plain($_GET['message_id']);
    db_query("DELETE FROM {spider_contacts_messages} WHERE id=:id", array(':id' => $message_id));
  }
  drupal_set_message(t('Message successfully deleted.'), 'status', FALSE);
  drupal_goto(url('admin/settings/spider_contacts/messages', array('absolute' => TRUE)));
}

/**
 * View message.
 */
function spider_contacts_message_edit() {
  if (isset($_GET['message_id'])) {
    $message_id = check_plain($_GET['message_id']);
    $row = db_query("SELECT * FROM {spider_contacts_messages} WHERE id=:id", array(':id' => $message_id))->fetchObject();
    $contact_row = db_query("SELECT * FROM {spider_contacts_contacts} WHERE id=:id", array(':id' => $row->to_contact))->fetchObject();
    $title = $row->title;
    $name = $contact_row->first_name . ' ' . $contact_row->last_name;
    $sender = $row->sender;
    $phone = $row->phone;
    $email = $row->email;
    $cont_pref = $row->cont_pref;
    $category = db_query("SELECT name FROM {spider_contacts_contacts_categories} WHERE id=:id", array(':id' => $contact_row->category_id))->fetchField();
    $date = $row->date;
    $text = $row->text;

    $form = array();
    $form['message'] = array(
      '#type' => 'submit',
      '#submit' => array('spider_contacts_messages_cancel'),
      '#value' => t('Back'),
      '#attributes' => array('style' => 'float:right;'),
      '#suffix' => '
        <label><h3>' . t('A Message from @sender', array('@sender' => $sender)) . '</h3></label>
        <table style="width:400px;border:1px solid;">
          <tr>
            <td style="width:100px;border:1px solid;"></td>
            <td style="border:1px solid;"><strong>' . t('Sender Details') . '</strong></td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('Sender Phone') . ':</td>
            <td style="border:1px solid;">' . $phone . '</td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('Sender Email') . ':</td>
            <td style="border:1px solid;">' . $email . '</td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('Sender Contact Preference') . ':</td>
            <td style="border:1px solid;">' . $cont_pref . '</td>
          </tr>
          <tr></tr>
          <tr>
            <td style="width:100px;border:1px solid;"></td>
            <td style="border:1px solid;"><strong>' . t('Message Details') . '</strong></td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('To Contact') . ':</td>
            <td style="border:1px solid;">' . $name . '</td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('Category') . ':</td>
            <td style="border:1px solid;">' . $category . '</td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('Date') . ':</td>
            <td style="border:1px solid;">' . $date . '</td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('Title') . ':</td>
            <td style="border:1px solid;">' . $title . '</td>
          </tr>
          <tr>
            <td style="width:100px;border:1px solid;text-align: right;">' . t('Message') . ':</td>
            <td style="border:1px solid;">' . $text . '</td>
          </tr>
        </table>',
    );
    db_query("UPDATE {spider_contacts_messages} SET readen=:readen WHERE id=:id", array(':readen' => 1, ':id' => $message_id));
    return $form;
  }
}

/**
 * Back to messages table.
 */
function spider_contacts_messages_cancel($form, &$form_state) {
  $form_state['redirect'] = url('admin/settings/spider_contacts/messages', array('absolute' => TRUE));
}
