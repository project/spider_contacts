/**
 * Select theme.
 */
function spider_contacts_set_theme() {
  themeID = document.getElementById('edit-default-themes').value;
  var if_set;
  switch (themeID) {
    case '1':
      if_set = spider_contacts_reset_theme_1();
      if (if_set) {
        document.getElementById("edit-theme-title").value = "new_Default theme";
      }
      break;
  }
}

/**
 * Reset default theme.
 */
function spider_contacts_reset_theme_1() {
  if (confirm(Drupal.t('Do you really want to reset theme?'))) {
    // Full View Options.
    document.getElementById("edit-radius-1").checked = true;
    document.getElementById("edit-radius-0").checked = false;
    document.getElementById("edit-parameters-select-box-width").value = "300";
    document.getElementById("edit-border-style").value = "solid";
    // If jscolor exists.
    if (document.getElementById("edit-module-background-color").color) {
      document.getElementById("edit-module-background-color").color.fromString("F0F0F0");
      document.getElementById("edit-params-background-color1").color.fromString("4AEAFF");
      document.getElementById("edit-params-background-color2").color.fromString("1ACDED");
      document.getElementById("edit-border-color").color.fromString("36739E");
      document.getElementById("edit-text-color").color.fromString("000000");
      document.getElementById("edit-params-color").color.fromString("000000");
      document.getElementById("edit-hyperlink-color").color.fromString("DEDACA");
      document.getElementById("edit-title-color").color.fromString("FFFFFF");
      document.getElementById("edit-title-background-color").color.fromString("00AEEF");
      document.getElementById("edit-button-background-color").color.fromString("00AEEF");
      document.getElementById("edit-button-color").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-module-background-color").value = "F0F0F0";
      document.getElementById("edit-params-background-color1").value = "4AEAFF";
      document.getElementById("edit-params-background-color2").value = "1ACDED";
      document.getElementById("edit-border-color").value = "36739E";
      document.getElementById("edit-text-color").value = "000000";
      document.getElementById("edit-params-color").value = "000000";
      document.getElementById("edit-hyperlink-color").value = "DEDACA";
      document.getElementById("edit-title-color").value = "FFFFFF";
      document.getElementById("edit-title-background-color").value = "00AEEF";
      document.getElementById("edit-button-background-color").value = "00AEEF";
      document.getElementById("edit-button-color").value = "FFFFFF";
    }
    document.getElementById("edit-border-width").value = "12";
    document.getElementById("edit-count-of-contact-in-the-row").value = "2";
    document.getElementById("edit-count-of-rows-in-the-page").value = "5";
    document.getElementById("edit-contact-cell-width").value = "300";
    document.getElementById("edit-contact-cell-height").value = "370";
    document.getElementById("edit-small-picture-width").value = "100";
    document.getElementById("edit-small-picture-height").value = "100";
    document.getElementById("edit-text-size-small").value = "12";
    document.getElementById("edit-title-size-small").value = "20";
    
    // Table View Options.
    // If jscolor exists.
    if (document.getElementById("edit-table-background-color").color) {
      document.getElementById("edit-table-background-color").color.fromString("F0F0F0");
      document.getElementById("edit-table-params-background-color1").color.fromString("00AEEF");
      document.getElementById("edit-table-params-background-color2").color.fromString("ED5353");
      document.getElementById("edit-table-border-color").color.fromString("36739E");
      document.getElementById("edit-table-text-color").color.fromString("050505");
      document.getElementById("edit-table-params-color1").color.fromString("FFFFFF");
      document.getElementById("edit-table-params-color2").color.fromString("000000");
      document.getElementById("edit-table-title-color").color.fromString("FFFFFF");
      document.getElementById("edit-table-title-background-color").color.fromString("3E95E6");
      document.getElementById("edit-hover-color").color.fromString("14C4FF");
      document.getElementById("edit-hover-text-color").color.fromString("00AEEF");
      document.getElementById("edit-table-button-background-color").color.fromString("00AEEF");
      document.getElementById("edit-table-button-color").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-table-background-color").value = "F0F0F0";
      document.getElementById("edit-table-params-background-color1").value = "00AEEF";
      document.getElementById("edit-table-params-background-color2").value = "ED5353";
      document.getElementById("edit-table-border-color").value = "36739E";
      document.getElementById("edit-table-text-color").value = "050505";
      document.getElementById("edit-table-params-color1").value = "FFFFFF";
      document.getElementById("edit-table-params-color2").value = "000000";
      document.getElementById("edit-table-title-color").value = "FFFFFF";
      document.getElementById("edit-table-title-background-color").value = "3E95E6";
      document.getElementById("edit-hover-color").value = "14C4FF";
      document.getElementById("edit-hover-text-color").value = "00AEEF";
      document.getElementById("edit-table-button-background-color").value = "00AEEF";
      document.getElementById("edit-table-button-color").value = "FFFFFF";
    }
    document.getElementById("edit-table-parameters-select-box-width").value = "200";
    document.getElementById("edit-table-border-style").value = "solid";
    document.getElementById("edit-table-radius-1").checked = true;
    document.getElementById("edit-table-radius-0").checked = false;
    document.getElementById("edit-table-border-width").value = "12";
    document.getElementById("edit-change-on-hover-1").checked = true;
    document.getElementById("edit-change-on-hover-0").checked = false;
    document.getElementById("edit-count-of-rows-in-the-table").value = "5";
    document.getElementById("edit-table-text-size-small").value = "12";
    document.getElementById("edit-table-title-size-small").value = "16";
    document.getElementById("edit-table-small-picture-width").value = "50";
    document.getElementById("edit-table-small-picture-height").value = "50";

    // Short View Options.
    // If jscolor exists.
    if (document.getElementById("edit-cube-background-color").color) {
      document.getElementById("edit-cube-background-color").color.fromString("F0F0F0");
      document.getElementById("edit-cube-border-color").color.fromString("FFFFFF");
      document.getElementById("edit-cube-text-color").color.fromString("000000");
      document.getElementById("edit-cube-hyperlink-color").color.fromString("049ACC");
      document.getElementById("edit-cube-title-color").color.fromString("FFFFFF");
      document.getElementById("edit-cube-title-background-color").color.fromString("00AEEF");
      document.getElementById("edit-cube-button-background-color").color.fromString("00AEEF");
      document.getElementById("edit-cube-button-color").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-cube-background-color").value = "F0F0F0";
      document.getElementById("edit-cube-border-color").value = "FFFFFF";
      document.getElementById("edit-cube-text-color").value = "000000";
      document.getElementById("edit-cube-hyperlink-color").value = "049ACC";
      document.getElementById("edit-cube-title-color").value = "FFFFFF";
      document.getElementById("edit-cube-title-background-color").value = "00AEEF";
      document.getElementById("edit-cube-button-background-color").value = "00AEEF";
      document.getElementById("edit-cube-button-color").value = "FFFFFF";
    }
    document.getElementById("edit-cube-radius-1").checked = true;
    document.getElementById("edit-cube-radius-0").checked = false;
    document.getElementById("edit-cube-border-style").value = "solid";
    document.getElementById("edit-cube-border-width").value = "8";

    // Contact Page Options.
    // If jscolor exists.
    if (document.getElementById("edit-viewcontact-background-color").color) {
      document.getElementById("edit-viewcontact-background-color").color.fromString("F0F0F0");
      document.getElementById("edit-viewcontact-border-color").color.fromString("36739E");
      document.getElementById("edit-viewcontact-params-background-color1").color.fromString("0BA7D6");
      document.getElementById("edit-viewcontact-params-background-color2").color.fromString("17D4FF");
      document.getElementById("edit-viewcontact-params-color").color.fromString("FFFFFF");
      document.getElementById("edit-viewcontact-text-color").color.fromString("FFFFFF");
      document.getElementById("edit-description-text-color").color.fromString("000000");
      document.getElementById("edit-viewcontact-title-background-color").color.fromString("00AEEF");
      document.getElementById("edit-viewcontact-title-color").color.fromString("FFFFFF");
      document.getElementById("edit-full-button-background-color").color.fromString("00AEEF");
      document.getElementById("edit-full-button-color").color.fromString("FFFFFF");
      document.getElementById("edit-messages-background-color").color.fromString("FFFFFF");
    }
    else {
      document.getElementById("edit-viewcontact-background-color").value = "F0F0F0";
      document.getElementById("edit-viewcontact-border-color").value = "36739E";
      document.getElementById("edit-viewcontact-params-background-color1").value = "0BA7D6";
      document.getElementById("edit-viewcontact-params-background-color2").value = "17D4FF";
      document.getElementById("edit-viewcontact-params-color").value = "FFFFFF";
      document.getElementById("edit-viewcontact-text-color").value = "FFFFFF";
      document.getElementById("edit-description-text-color").value = "000000";
      document.getElementById("edit-viewcontact-title-background-color").value = "00AEEF";
      document.getElementById("edit-viewcontact-title-color").value = "FFFFFF";
      document.getElementById("edit-full-button-background-color").value = "00AEEF";
      document.getElementById("edit-full-button-color").value = "FFFFFF";
      document.getElementById("edit-messages-background-color").value = "FFFFFF";
    }
    document.getElementById("edit-viewcontact-border-style").value = "solid";
    document.getElementById("edit-viewcontact-border-width").value = "12";
    document.getElementById("edit-large-picture-width").value = "100";
    document.getElementById("edit-large-picture-height").value = "100";
    document.getElementById("edit-small-pic-size").value = "60";
    document.getElementById("edit-text-size-big").value = "14";
    document.getElementById("edit-title-size-big").value = "16";
    document.getElementById("edit-viewcontact-radius-1").checked = true;
    document.getElementById("edit-viewcontact-radius-0").checked = false;
    return true;
  }
  else {
    return false;
  }
}
